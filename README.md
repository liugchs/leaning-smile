# leaning-smile

#### 简介
分布式、微服务
#### 目录介绍

##### smile-common 服务通用定义（接口，返回，工具类等）

##### smile-mbg mybatisPlus对象映射模块

##### smile-security 通用安全模块

##### smile-admin 后台管理 包含 spring-security rabbitmq quartz mybatisPlus 等

##### smile-admin-service 监控中心服务

##### smile-user-service  用户服务 被监控中心监控

##### smile-config-service 配置中心服务

##### smile-eureka-service 注册中心服务

##### smile-feign-service feign服务调用服务

##### smile-gateway-service 网关服务

##### smile-nacos-user-service nacos用户服务

##### smile-nacos-config-client nacos配置客户端服务

##### smile-misc-oauth2 OAuth网关安全认证


##### smile-nacos-user-service dubbo + nacos(服务提供方)
##### smile-nacos-consumer-service dubbo + nacos(服务消费端（dubbo），分布式配置中心使用)

#### 软件架构
简单微服务学习


#### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request




