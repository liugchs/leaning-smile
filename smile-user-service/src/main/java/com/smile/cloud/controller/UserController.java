package com.smile.cloud.controller;


import com.smile.cloud.common.base.result.CommonResult;
import com.smile.cloud.common.model.User;

import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.util.*;


@RestController
@RequestMapping("/v1")
@Slf4j
public class UserController {

    private Map<String, User> userMap = new HashMap<>();

    {
        for (int i = 0; i < 5; i++) {
            User user = new User();
            user.setId(i);
            user.setMail("qq" + i + "@163.com");
            user.setName("smile" + i);
            user.setRegDate(new Date());
            userMap.put(i + "", user);
        }
    }

    @ApiOperation(value = "用户id查询用户", notes = "private")
    @GetMapping(value = "/users/{userId}")
    public CommonResult<User> getUser(@PathVariable String userId) {
        User user = (User) userMap.get(userId);
        log.info("按用户id查询，用户名称为：{}", user.getName());
        return CommonResult.success(user);
    }


    @ApiOperation(value = "查询所有用户")
    @GetMapping(value = "/users")
    public CommonResult<List<User>> allUsers() {
        List<User> users = new ArrayList<>(userMap.values());
        return CommonResult.success(users);
    }

}