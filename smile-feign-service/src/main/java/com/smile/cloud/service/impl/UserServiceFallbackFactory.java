package com.smile.cloud.service.impl;

import com.smile.cloud.common.model.User;
import com.smile.cloud.UserServiceClient;
import feign.hystrix.FallbackFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class UserServiceFallbackFactory implements FallbackFactory<UserServiceClient> {
    @Override
    public UserServiceClient create(final Throwable throwable) {
        return new UserServiceClient() {
            @Override
            public User getUser(String userId) {
                log.warn("Fallback reason={}, userId={}", throwable.getMessage(), userId);
                return null;
            }

            @Override
            public User getAllUser() {
                log.warn("Fallback reason={}", throwable.getMessage());
                return null;
            }

        };
    }

}