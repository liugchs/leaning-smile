package com.smile.cloud;

import com.smile.cloud.common.model.User;
import com.smile.cloud.service.impl.UserServiceFallbackFactory;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient(value = "smile-user-service", path = "/v1", fallbackFactory = UserServiceFallbackFactory.class)
public interface UserServiceClient {

    @RequestMapping(value = "/users/{userId}", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    User getUser(@PathVariable(value = "userId") String userId);

    @RequestMapping(value = "/users", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    User getAllUser();
}




