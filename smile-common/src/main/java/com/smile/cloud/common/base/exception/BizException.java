package com.smile.cloud.common.base.exception;

import com.smile.cloud.common.base.result.IResultCode;
import lombok.Getter;

/**
 * @author LGC
 */
@Getter
public class BizException extends RuntimeException {
    /**
     * 业务异常类型
     */
    private IResultCode resultCode;

    /**
     * 异常msg
     */
    private String msg;

    public BizException(IResultCode resultCode) {
        super(resultCode.getMsg());
        this.resultCode = resultCode;
        this.msg = resultCode.getMsg();
    }

    public BizException(IResultCode resultCode, String msg) {
        super(msg);
        this.resultCode = resultCode;
        this.msg = msg;
    }

    public BizException(IResultCode resultCode, Throwable throwable) {
        super(throwable.getMessage(),throwable);
        this.resultCode = resultCode;
        this.msg = resultCode.getMsg();
    }

    public BizException(IResultCode resultCode, String msg, Throwable throwable) {
        super(msg, throwable);
        this.resultCode = resultCode;
        this.msg = msg;
    }

    public BizException(IResultCode resultCode, String code, String msg) {
        super(String.format("%s：%s", code, msg));
        this.resultCode = resultCode;
        this.msg = String.format("%s：%s", code, msg);
    }

}
