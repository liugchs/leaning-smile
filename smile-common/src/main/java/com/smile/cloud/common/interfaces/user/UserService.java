package com.smile.cloud.common.interfaces.user;

import com.smile.cloud.common.model.User;

/**
 * @author smile
 * @version V1.0
 * @className UserService
 * @description TODO
 * @date 2021/4/26
 **/
public interface UserService {
    /**
     * 获取用户信息
     *
     * @param userId
     * @return
     */
    User getUserInfo(Integer userId);

}
