package com.smile.cloud.common.base.result;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * @author smile
 */
@Getter
@NoArgsConstructor
@AllArgsConstructor
public enum BaseResultEnum implements IResultCode {
    //2XX 请求成功
    SUCCESS(200, "执行成功"),
    REFRESH_TOKEN(201, "续签成功"),

    //4xx 资源权限异常
    UNAUTHORIZED(401, "暂未登录或token已经过期"),
    NO_PERMISSION(403, "没有相关权限,禁止访问"),
    TIME_OUT(408, "请求超时"),
    ACCESS_LIMIT(409, "访问限制"),
    USERNAME_PASSWORD_ERROR(411, "账号或密码错误"),
    NEED_AUTHORIZE(412, "需要授权"),
    //5xx 请求错误(业务)
    FAILURE(500, "执行失败"),
    VALIDATE(501, "参数错误"),
    UNKONW_ERROR(555, "未知错误"),

    SQL_DUPLICATE_KEY(560, "数据重复");

    private Integer code;
    private String msg;


}
