package com.smile.cloud.common.base.service;

import org.springframework.beans.BeanUtils;

import java.util.List;
import java.util.function.BiConsumer;
import java.util.stream.Collectors;

/**
 * 对象转换服务
 *
 * @author smile
 **/
public class ConvertService {


    public <S, D> void convert(S srcObject, D destObject) {
        BeanUtils.copyProperties(srcObject, destObject);
    }

    public <S, D> void convert(S srcObject, D destObject, BiConsumer<S, D> consumer) {
        BeanUtils.copyProperties(srcObject, destObject);
        consumer.accept(srcObject, destObject);
    }

    public <S, D> D convert(S srcObject, Class<D> destClassType) {
        D dest = null;
        if (srcObject == null) {
            return null;
        }
        try {
            dest = destClassType.newInstance();
            BeanUtils.copyProperties(srcObject, dest);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return dest;
    }

    public <S, D> D convert(S srcObject, Class<D> destClassType, BiConsumer<S, D> consumer) {
        D dest = convert(srcObject, destClassType);
        consumer.accept(srcObject, dest);
        return dest;
    }

    public <S, D> List<D> convertList(List<S> srcObjectList, Class<D> destClassType) {
        return srcObjectList.stream().map(srcObject -> convert(srcObject, destClassType)).collect(Collectors.toList());

    }


    public <S, D> List<D> convertList(List<S> srcObjectList, Class<D> destClassType, BiConsumer<S, D> consumer) {
        return srcObjectList.stream().map(srcObject -> convert(srcObject, destClassType, consumer)).collect(Collectors.toList());
    }

}