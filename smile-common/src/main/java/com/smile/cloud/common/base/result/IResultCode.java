package com.smile.cloud.common.base.result;

/**
 * 封装API的错误码
 */
public interface IResultCode {
    /**
     * 状态码
     *
     * @return
     */
    Integer getCode();

    /**
     * 消息
     *
     * @return
     */
    String getMsg();
}
