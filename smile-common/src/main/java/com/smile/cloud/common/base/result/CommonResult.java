package com.smile.cloud.common.base.result;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 通用返回对象
 *
 * @author LGC
 * @date 2019/4/19
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CommonResult<T> {
    private long code;
    private String msg;
    private T data;

    /**
     * 成功返回结果
     */
    public static <T> CommonResult<T> success() {
        return success(null);
    }

    /**
     * 成功返回结果
     *
     * @param data 获取的数据
     */
    public static <T> CommonResult<T> success(T data) {
        return new CommonResult<T>(BaseResultEnum.SUCCESS.getCode(), BaseResultEnum.SUCCESS.getMsg(), data);
    }

    /**
     * 成功返回结果
     *
     * @param data    获取的数据
     * @param message 提示信息
     */
    public static <T> CommonResult<T> success(T data, String message) {
        return new CommonResult<T>(BaseResultEnum.SUCCESS.getCode(), message, data);
    }

    /**
     * 失败返回结果
     *
     * @param resultCode 错误码
     */
    public static <T> CommonResult<T> failed(IResultCode resultCode) {
        return new CommonResult<T>(resultCode.getCode(), resultCode.getMsg(), null);
    }

    /**
     * 失败返回结果
     *
     * @param resultCode 错误码
     * @param message    错误信息
     */
    public static <T> CommonResult<T> failed(IResultCode resultCode, String message) {
        return new CommonResult<T>(resultCode.getCode(), message, null);
    }

    /**
     * 失败返回结果
     *
     * @param message 提示信息
     */
    public static <T> CommonResult<T> failed(String message) {
        return new CommonResult<T>(BaseResultEnum.FAILURE.getCode(), message, null);
    }

    /**
     * 失败返回结果
     */
    public static <T> CommonResult<T> failed() {
        return failed(BaseResultEnum.FAILURE);
    }

    /**
     * 参数验证失败返回结果
     */
    public static <T> CommonResult<T> validateFailed() {
        return failed(BaseResultEnum.VALIDATE);
    }

    /**
     * 参数验证失败返回结果
     *
     * @param message 提示信息
     */
    public static <T> CommonResult<T> validateFailed(String message) {
        return new CommonResult<T>(BaseResultEnum.VALIDATE.getCode(), message, null);
    }

    /**
     * 暂未登录或token已经过期
     */
    public static <T> CommonResult<T> unauthorized(T data) {
        return new CommonResult<T>(BaseResultEnum.UNAUTHORIZED.getCode(), BaseResultEnum.UNAUTHORIZED.getMsg(), data);
    }

    /**
     * 未授权返回结果
     */
    public static <T> CommonResult<T> noPermission(T data) {
        return new CommonResult<T>(BaseResultEnum.NO_PERMISSION.getCode(), BaseResultEnum.NO_PERMISSION.getMsg(), data);
    }

}