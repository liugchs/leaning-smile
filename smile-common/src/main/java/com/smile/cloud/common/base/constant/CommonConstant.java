package com.smile.cloud.common.base.constant;

/**
 * @author LGC
 */
public class CommonConstant {

    //日志链路追踪id 日志标志
    public static final String LOG_TRACE_ID = "traceId";
    //日志链路最终id信息头
    public static final String TRACE_ID_HEADER = "x-trace-header";
}
