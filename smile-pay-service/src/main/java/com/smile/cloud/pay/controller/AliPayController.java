package com.smile.cloud.pay.controller;

import com.smile.cloud.common.base.result.CommonResult;
import com.smile.cloud.common.base.exception.BizException;
import com.smile.cloud.pay.common.base.AliBaseParemIsNull;
import com.smile.cloud.pay.common.pojo.AliPayRefundVO;
import com.smile.cloud.pay.common.pojo.AliPayVO;
import com.smile.cloud.pay.common.pojo.AliTradePayVO;
import com.smile.cloud.pay.service.AliPayService;
import com.smile.cloud.pay.util.QRCodeUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

/**
 * 支付宝支付相关接口
 */
@RestController
@RequestMapping("/payment/alipay")
public class AliPayController extends AliBaseParemIsNull {

    @Autowired
    private AliPayService aliPayService;

    @GetMapping("/aa")
    public String a() {
        return "111";
    }

    /**
     * 电脑网站支付
     *
     * @param aliPayVO
     * @return
     */
    @PostMapping("/pcPay")
    public void pcPay(@RequestBody AliPayVO aliPayVO) {
        AmountIsNull(aliPayVO.getAmount());
        aliPayService.pcPay(aliPayVO);
    }

    /**
     * 手机网站支付
     *
     * @param aliPayVO
     */
    @PostMapping("/wapPay")
    public void wapPay(@RequestBody AliPayVO aliPayVO) {
        AmountIsNull(aliPayVO.getAmount());
        aliPayService.wapPay(aliPayVO);
    }

    /**
     * app支付
     *
     * @param aliPayVO
     */
    @PostMapping("/appPay")
    public CommonResult<String> appPay(@RequestBody AliPayVO aliPayVO) {
        AmountIsNull(aliPayVO.getAmount());
        String str = aliPayService.appPay(aliPayVO);
        return CommonResult.success(str);
    }

    /**
     * 条码 声波支付  --> 商家扫码个人
     *
     * @param aliTradePayVO
     * @return
     */
    @PostMapping("/tradePay")
    public CommonResult<String> tradePay(@RequestBody AliTradePayVO aliTradePayVO) {
        paramIsNull(aliTradePayVO);
        String response = aliPayService.tradePay(aliTradePayVO);
        return CommonResult.success(response);
    }

    /**
     * 扫码支付  -->  个人扫码商家
     *
     * @param aliTradePayVO
     * @return
     */
    @PostMapping("/tradePrecreatePay")
    public CommonResult<String> tradePrecreatePay(@RequestBody AliTradePayVO aliTradePayVO) {
        storeIdIsNull(aliTradePayVO);
        String urlStr = aliPayService.tradePrecreatePay(aliTradePayVO);
        try {
            String qrCode = QRCodeUtil.createQRCode(urlStr, 500, 500);
            return CommonResult.success(qrCode);
        } catch (IOException e) {
            e.printStackTrace();
            throw new BizException(null, "二维码生成失败");
        }
    }

    /**
     * 退款
     *
     * @param aliPayRefundVO
     * @return
     */
    @PostMapping("/tradeRefund")
    public CommonResult<String> tradeRefund(@RequestBody AliPayRefundVO aliPayRefundVO) {
        refundIsNull(aliPayRefundVO);
        String str = aliPayService.tradeRefund(aliPayRefundVO);
        return CommonResult.success(str);
    }

    /**
     * 交易订单查询
     *
     * @param aliPayRefundVO
     * @return
     */
    @PostMapping("/tradeQuery")
    public CommonResult<String> tradeQuery(@RequestBody AliPayRefundVO aliPayRefundVO) {
        refundTwoIsNull(aliPayRefundVO);
        String str = aliPayService.tradeQuery(aliPayRefundVO);
        return CommonResult.success(str);
    }

}
