package com.smile.cloud.pay.common.pojo;

import lombok.Data;

import java.math.BigDecimal;

/**
 * pcPay wapPay appPay入参
 */
@Data
public class AliPayVO {

    //订单金额
    private BigDecimal amount;
}
