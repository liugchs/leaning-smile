package com.smile.cloud.pay.service;


import com.smile.cloud.pay.common.pojo.AliPayRefundVO;
import com.smile.cloud.pay.common.pojo.AliPayVO;
import com.smile.cloud.pay.common.pojo.AliTradePayVO;

public interface AliPayService {

    //阿里电脑网站支付
    void pcPay(AliPayVO aliPayVO);

    //手机网站支付
    void wapPay(AliPayVO aliPayVO);

    //阿里app支付
    String appPay(AliPayVO aliPayVO);

    //条码 声波支付  --> 商家扫码个人
    String tradePay(AliTradePayVO aliTradePayVO);

    //扫码支付  -->  个人扫码商家
    String tradePrecreatePay(AliTradePayVO aliTradePayVO);

    //退款
    String tradeRefund(AliPayRefundVO aliPayRefundVO);

    //交易订单查询
    String tradeQuery(AliPayRefundVO aliPayRefundVO);
}
