package com.smile.cloud.pay.common.base;


import com.smile.cloud.common.base.exception.BizException;
import com.smile.cloud.pay.common.pojo.AliPayRefundVO;
import com.smile.cloud.pay.common.pojo.AliTradePayVO;
import org.apache.commons.lang3.StringUtils;

import java.math.BigDecimal;

public class AliBaseParemIsNull {

    //金额校验
    public void AmountIsNull(BigDecimal amount) {
        if (amount == null) {
            throw new BizException(null, "金额为空");
        }
    }


    //商户门店编号和金额校验
    public void storeIdIsNull(AliTradePayVO aliTradePayVO) {
        if (StringUtils.isEmpty(aliTradePayVO.getStoreId())) {
            throw new BizException(null, "商户门店编号为空");
        }
        AmountIsNull(aliTradePayVO.getAmount());
    }

    //金额 授权码 商户编号 条码
    public void paramIsNull(AliTradePayVO aliTradePayVO) {
        if (StringUtils.isEmpty(aliTradePayVO.getAuthCode())) {
            throw new BizException(null, "授权码为空");
        }
        if (StringUtils.isEmpty(aliTradePayVO.getScene())) {
            throw new BizException(null, "条码为空");
        }
        storeIdIsNull(aliTradePayVO);
    }

    public void refundIsNull(AliPayRefundVO aliPayRefundVO) {
        if (StringUtils.isEmpty(aliPayRefundVO.getOutTradeNo()) && StringUtils.isEmpty(aliPayRefundVO.getTradeNo())) {
            throw new BizException(null, "订单号为空");
        }
        if (StringUtils.isEmpty(aliPayRefundVO.getFlag())) {
            throw new BizException(null, "是否全额退款标识为空");
        }
        AmountIsNull(aliPayRefundVO.getAmount());
    }

    public void refundTwoIsNull(AliPayRefundVO aliPayRefundVO) {
        if (StringUtils.isEmpty(aliPayRefundVO.getOutTradeNo()) && StringUtils.isEmpty(aliPayRefundVO.getTradeNo())) {
            throw new BizException(null, "订单号为空");
        }
    }
}
