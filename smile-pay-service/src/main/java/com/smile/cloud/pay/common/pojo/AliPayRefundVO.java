package com.smile.cloud.pay.common.pojo;

import lombok.Data;

@Data
public class AliPayRefundVO extends AliPayVO {

    //订单号
    private String outTradeNo;

    //支付宝交易号
    private String tradeNo;

    //0 : 全额退款  1 : 部分退款
    private String flag;
}
