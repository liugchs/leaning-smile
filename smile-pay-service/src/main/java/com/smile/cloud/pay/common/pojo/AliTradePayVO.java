package com.smile.cloud.pay.common.pojo;

import lombok.Data;

/**
 * tradePay入参
 */
@Data
public class AliTradePayVO extends AliPayVO {

    //授权码
    private String authCode;

    //支付场景，条码：bar_code，声波：wave_code
    private String scene;

    //商户门店编号
    private String storeId;
}
