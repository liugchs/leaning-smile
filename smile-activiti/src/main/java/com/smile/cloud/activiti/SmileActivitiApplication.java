package com.smile.cloud.activiti;

import org.activiti.core.common.spring.identity.config.ActivitiSpringIdentityAutoConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author LGC
 */
@SpringBootApplication(exclude = {ActivitiSpringIdentityAutoConfiguration.class})
public class SmileActivitiApplication {

    public static void main(String[] args) {
        SpringApplication.run(SmileActivitiApplication.class, args);
    }

}
