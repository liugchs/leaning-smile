package com.smile.cloud.activiti.service;

import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author LGC
 */
@Service
public class MyService {

    @Autowired
    private RuntimeService runtimeService;

    @Autowired
    private TaskService taskService;

	@Transactional
    public void startProcess() {
        ProcessInstance processInstance = runtimeService.startProcessInstanceByKey("holiday");
        System.out.println("流程启动成功，流程id:" + processInstance.getId());
        System.out.println("流程启动成功，流程definitionName:" + processInstance.getProcessDefinitionName());
        System.out.println("流程启动成功，流程businessKey:" + processInstance.getBusinessKey());
    }

	@Transactional
    public List<Task> getTasks(String assignee) {
        return taskService.createTaskQuery() .processDefinitionKey("holiday").list();
    }

}