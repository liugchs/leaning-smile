package com.smile.cloud.activiti.controller;

import com.smile.cloud.activiti.service.MyService;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.activiti.engine.ProcessEngine;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.repository.Deployment;
import org.activiti.engine.task.Task;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;


/**
 * @author LGC
 */
@Slf4j
@RestController
public class ActivitiController {

    @Resource
    private MyService myService;

    @Resource
    private ProcessEngine processEngine;

    /**
     * 流程定义的部署
     */
    @PostMapping(value = "/createDeploy")
    public void createDeploy() {
        RepositoryService repositoryService = processEngine.getRepositoryService();
        Deployment deployment = repositoryService.createDeployment()
                .addClasspathResource("processes/holiday.bpmn")//添加bpmn资源
                .addClasspathResource("processes/holiday.png")
                .name("请假申请单流程")
                .deploy();

        log.info("流程部署id:" + deployment.getName());
        log.info("流程部署名称:" + deployment.getId());
    }

    @PostMapping(value = "/process")
    public void startProcessInstance() {
        myService.startProcess();
    }

    @GetMapping(value = "/tasks")
    public List<Task> getTasks(@RequestParam String assignee) {
        List<Task> tasks = myService.getTasks(assignee);
//        List<TaskRepresentation> dtos = new ArrayList<>();
//        for (Task task : tasks) {
//            dtos.add(new TaskRepresentation(task.getId(), task.getName()));
//        }
        return tasks;
    }

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    static class TaskRepresentation {
        private String id;
        private String name;
    }
}
