package com.smile.cloud.setaAcount.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.math.BigDecimal;

import lombok.Data;

/**
 * @author LGC
 * @date 2021/8/18 10:21
 * @copyright 2021 mofang. All rights reserved
 */

@ApiModel(value = "com-smile-cloud-setaAcount-model-Account")
@Data
@TableName(value = "account")
public class Account implements Serializable {
    /**
     * id
     */
    @TableId(value = "id", type = IdType.AUTO)
    @ApiModelProperty(value = "id")
    private Long id;

    /**
     * 用户id
     */
    @TableField(value = "user_id")
    @ApiModelProperty(value = "用户id")
    private Long userId;

    /**
     * 总额度
     */
    @TableField(value = "total")
    @ApiModelProperty(value = "总额度")
    private BigDecimal total;

    /**
     * 已用余额
     */
    @TableField(value = "used")
    @ApiModelProperty(value = "已用余额")
    private BigDecimal used;

    /**
     * 剩余可用额度
     */
    @TableField(value = "residue")
    @ApiModelProperty(value = "剩余可用额度")
    private BigDecimal residue;

    private static final long serialVersionUID = 1L;
}