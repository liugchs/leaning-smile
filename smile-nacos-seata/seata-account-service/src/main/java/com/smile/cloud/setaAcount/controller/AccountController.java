package com.smile.cloud.setaAcount.controller;

import com.smile.cloud.common.base.result.CommonResult;
import com.smile.cloud.setaAcount.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;

@RestController
@RequestMapping("/account")
public class AccountController {

    @Autowired
    private AccountService accountService;

    /**
     * 扣减账户余额
     */
    @GetMapping("/decrease")
    public CommonResult<String> decrease(@RequestParam("userId") Long userId, @RequestParam("money") BigDecimal money) {
        accountService.decrease(userId, money);
        return CommonResult.success("扣减账户余额成功！");
    }
}
