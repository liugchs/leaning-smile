package com.smile.cloud.setaAcount.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.smile.cloud.setaAcount.model.Account;

import java.math.BigDecimal;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 * @author LGC
 * @date 2021/8/18 10:21
 * @copyright 2021 mofang. All rights reserved
 */

public interface AccountMapper extends BaseMapper<Account> {
    int updateBatch(List<Account> list);

    int updateBatchSelective(List<Account> list);

    int batchInsert(@Param("list") List<Account> list);


    /**
     * 扣减账户余额
     */
    void decrease(@Param("userId") Long userId, @Param("money") BigDecimal money);


}