package com.smile.cloud.setaStorage.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import lombok.Data;

/**
 * @author LGC
 * @date 2021/8/18 10:41
 * @copyright 2021 mofang. All rights reserved
 */

@ApiModel(value = "com-smile-cloud-seataStorage-model-Storage")
@Data
@TableName(value = "`storage`")
public class Storage implements Serializable {
    @TableId(value = "id", type = IdType.AUTO)
    @ApiModelProperty(value = "")
    private Long id;

    /**
     * 产品id
     */
    @TableField(value = "product_id")
    @ApiModelProperty(value = "产品id")
    private Long productId;

    /**
     * 总库存
     */
    @TableField(value = "total")
    @ApiModelProperty(value = "总库存")
    private Integer total;

    /**
     * 已用库存
     */
    @TableField(value = "used")
    @ApiModelProperty(value = "已用库存")
    private Integer used;

    /**
     * 剩余库存
     */
    @TableField(value = "residue")
    @ApiModelProperty(value = "剩余库存")
    private Integer residue;

    private static final long serialVersionUID = 1L;
}