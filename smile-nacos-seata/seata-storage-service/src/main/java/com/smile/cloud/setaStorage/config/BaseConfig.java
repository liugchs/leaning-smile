package com.smile.cloud.setaStorage.config;


import com.smile.cloud.common.base.service.ConvertService;
import com.smile.cloud.common.base.util.SpringUtil;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author smile
 * @version V1.0
 * @className BaseConfig
 * @description
 * @date 2021/1/4
 **/
@Configuration
public class BaseConfig {

    /**
     * ConvertService 加载对象复制工具类spring
     *
     * @return
     */
    @Bean
    public ConvertService getConvertService() {
        return new ConvertService();
    }

    /**
     * SpringUtil 工具
     *
     * @return
     */
    @Bean
    public SpringUtil getSpringUtil() {
        return new SpringUtil();
    }

}
