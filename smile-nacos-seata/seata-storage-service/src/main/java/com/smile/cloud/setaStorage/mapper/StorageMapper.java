package com.smile.cloud.setaStorage.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.smile.cloud.setaStorage.model.Storage;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 * @author LGC
 * @date 2021/8/18 10:41
 * @copyright 2021 mofang. All rights reserved
 */

public interface StorageMapper extends BaseMapper<Storage> {
    int updateBatch(List<Storage> list);

    int updateBatchSelective(List<Storage> list);

    int batchInsert(@Param("list") List<Storage> list);

    /**
     * 扣减库存
     */
    void decrease(@Param("productId") Long productId, @Param("count") Integer count);
}