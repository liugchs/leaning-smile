package com.smile.cloud.setaOrder.controller;

import com.smile.cloud.common.base.result.CommonResult;
import com.smile.cloud.setaOrder.model.Order;
import com.smile.cloud.setaOrder.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/order")
public class OrderController {

    @Autowired
    private OrderService orderService;

    /**
     * 创建订单
     */
    @GetMapping("/create")
    public CommonResult<String> create(Order order) throws Exception {
        orderService.create(order);
        return CommonResult.success("订单创建成功!");
    }
}
