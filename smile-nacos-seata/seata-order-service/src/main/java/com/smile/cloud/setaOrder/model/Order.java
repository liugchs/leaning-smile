package com.smile.cloud.setaOrder.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import lombok.Data;

/**
 *
 * @author LGC
 * @date 2021/8/18 10:58
 * @copyright 2021 mofang. All rights reserved
 */

@ApiModel(value="com-smile-cloud-setaOrder-model-Order")
@Data
@TableName(value = "`order`")
public class Order implements Serializable {
    @TableId(value = "id", type = IdType.AUTO)
    @ApiModelProperty(value="")
    private Long id;

    /**
     * 用户id
     */
    @TableField(value = "user_id")
    @ApiModelProperty(value="用户id")
    private Long userId;

    /**
     * 产品id
     */
    @TableField(value = "product_id")
    @ApiModelProperty(value="产品id")
    private Long productId;

    /**
     * 数量
     */
    @TableField(value = "`count`")
    @ApiModelProperty(value="数量")
    private Integer count;

    /**
     * 金额
     */
    @TableField(value = "money")
    @ApiModelProperty(value="金额")
    private Long money;

    /**
     * 订单状态：0：创建中；1：已完结
     */
    @TableField(value = "`status`")
    @ApiModelProperty(value="订单状态：0：创建中；1：已完结")
    private Integer status;

    private static final long serialVersionUID = 1L;
}