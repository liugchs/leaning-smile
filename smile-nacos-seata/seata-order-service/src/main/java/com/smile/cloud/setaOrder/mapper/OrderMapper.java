package com.smile.cloud.setaOrder.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.smile.cloud.setaOrder.model.Order;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 *
 * @author LGC
 * @date 2021/8/18 10:58
 * @copyright 2021 mofang. All rights reserved
 */

public interface OrderMapper extends BaseMapper<Order> {
    int updateBatch(List<Order> list);

    int updateBatchSelective(List<Order> list);

    int batchInsert(@Param("list") List<Order> list);

    /**
     * 创建订单
     */
    void create(Order order);

    /**
     * 修改订单金额
     */
    void update(@Param("userId") Long userId, @Param("status") Integer status);
}