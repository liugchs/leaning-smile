package com.smile.cloud.setaOrder.service;


import com.smile.cloud.setaOrder.model.Order;

public interface OrderService {

    /**
     * 创建订单
     */
    void create(Order order) throws Exception;
}
