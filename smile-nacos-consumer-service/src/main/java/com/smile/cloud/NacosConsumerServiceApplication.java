package com.smile.cloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @author LGC
 */
@EnableDiscoveryClient
@SpringBootApplication
public class NacosConsumerServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(NacosConsumerServiceApplication.class, args);
	}

}
