package com.smile.cloud.userConsumer.controller;


import com.smile.cloud.common.model.User;
import com.smile.cloud.common.base.result.CommonResult;
import com.smile.cloud.common.interfaces.user.UserService;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author smile
 */
@Slf4j
@RestController
@RequestMapping("/manage/user")
public class UserController {

    @DubboReference
    private UserService userService;

    private Map<String, User> userMap = new HashMap<>();

    {
        for (int i = 0; i < 5; i++) {
            User user = new User();
            user.setId(i);
            user.setMail("qq" + i + "@163.com");
            user.setName("smile" + i);
            user.setRegDate(new Date());
            userMap.put(i + "", user);
        }
    }

    @ApiOperation(value = "用户id查询用户", notes = "用户id查询用户")
    @GetMapping(value = "/one/{id}")
    public CommonResult<User> getUser(@PathVariable Integer id) {
        User userInfo = userService.getUserInfo(id);
        return CommonResult.success(userInfo);
    }


//    @ApiOperation(value = "用户id查询用户", notes = "用户id查询用户")
//    @PostMapping(value = "/one")
//    @SentinelResource(value = "/manage/user/one", blockHandler = "exceptionHandler", blockHandlerClass = BlockExceptionUtils.class)
//    public DataResult<User> getUser(@RequestBody ID id) {
//        User userInfo = userService.getUserInfo(id.getId());
//        log.error("用户id查询用户ok");
//        return dataResult(userInfo);
//    }


}

