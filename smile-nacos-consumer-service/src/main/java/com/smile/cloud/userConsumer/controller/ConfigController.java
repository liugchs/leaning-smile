package com.smile.cloud.userConsumer.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;


@Slf4j
@RestController
@RefreshScope
public class ConfigController {

    @Value("${common}")
    private String common;

    @Value("${smile.service.config.name}")
    private String configName;

    @GetMapping("/commonInfo")
    public String commonInfo() {
        log.error("错误日志");
        return common;
    }

    @GetMapping("/configName")
    public String configName() {
        return configName;
    }
}
