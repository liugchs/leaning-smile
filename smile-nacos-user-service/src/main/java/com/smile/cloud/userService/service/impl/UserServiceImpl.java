package com.smile.cloud.userService.service.impl;

import com.smile.cloud.common.interfaces.user.UserService;
import com.smile.cloud.common.model.User;
import org.apache.dubbo.config.annotation.DubboService;

import java.util.*;

/**
 * @author smile
 * @version V1.0
 * @className UserServiceImpl
 * @description TODO
 * @date 2021/4/26
 **/
@DubboService
public class UserServiceImpl implements UserService {
    private Map<String, User> userMap = new HashMap<>();

    {
        for (int i = 0; i < 5; i++) {
            User user = new User();
            user.setId(i);
            user.setMail("qq" + i + "@163.com");
            user.setName("smile" + i);
            user.setRegDate(new Date());
            userMap.put(i + "", user);
        }
    }

    @Override
//    @SentinelResource(value = "getUserInfo")
    public User getUserInfo(Integer userId) {
        return userMap.get(userId.toString());
    }

}
