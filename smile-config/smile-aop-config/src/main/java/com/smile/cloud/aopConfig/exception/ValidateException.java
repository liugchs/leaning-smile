//package com.smile.cloud.aopConfig.exception;
//
//
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.context.support.DefaultMessageSourceResolvable;
//import org.springframework.http.HttpStatus;
//import org.springframework.http.converter.HttpMessageNotReadableException;
//import org.springframework.validation.BindException;
//import org.springframework.web.HttpMediaTypeNotSupportedException;
//import org.springframework.web.HttpRequestMethodNotSupportedException;
//import org.springframework.web.bind.MethodArgumentNotValidException;
//import org.springframework.web.bind.MissingServletRequestParameterException;
//import org.springframework.web.bind.annotation.ControllerAdvice;
//import org.springframework.web.bind.annotation.ExceptionHandler;
//import org.springframework.web.bind.annotation.ResponseBody;
//import org.springframework.web.bind.annotation.ResponseStatus;
//
//import javax.validation.ConstraintViolation;
//import javax.validation.ConstraintViolationException;
//import javax.xml.bind.ValidationException;
//import java.util.stream.Collectors;
//
//
///**
// * @author Xy.wang
// * @Description systemLog controller 参数校验异常处理类
// * @module star-sand
// * @date 2021/7/22 11:42
// * @copyright 2021 mofang. All rights reserved
// */
//
//@Slf4j
//@ResponseBody
//@ControllerAdvice
//public class ValidateException {
//
//    /**
//     * 处理请求参数格式错误 @RequestBody上validate失败后抛出的异常是MethodArgumentNotValidException异常
//     */
//
//
//    @ExceptionHandler(MethodArgumentNotValidException.class)
//    public CommonResult<String> methodArgumentNotValidExceptionHandler(MethodArgumentNotValidException e) {
//        log.error("参数验证失败", e);
//        String message = e.getBindingResult().getAllErrors().stream().map(DefaultMessageSourceResolvable::getDefaultMessage).collect(
//                Collectors.joining());
//        return new CommonResult<String>().fail(message);
//    }
//
//    /**
//     * 处理Get请求中 使用@Valid 验证路径中请求实体校验失败后抛出的异常
//     */
//
//
//    @ExceptionHandler(BindException.class)
//    public CommonResult<String> bindExceptionHandler(BindException e) {
//        log.error("参数绑定失败", e);
//        String message = e.getBindingResult().getAllErrors().stream().map(DefaultMessageSourceResolvable::getDefaultMessage).collect(
//                Collectors.joining());
//        return new CommonResult<String>().fail(message);
//    }
//
//    /**
//     * 处理请求参数格式错误 @RequestParam上validate失败后抛出的异常是javax.validation.ConstraintViolationException
//     */
//
//
//    @ExceptionHandler(ConstraintViolationException.class)
//    public CommonResult<String> constraintViolationExceptionHandler(ConstraintViolationException e) {
//        log.error("参数验证失败", e);
//        String message = e.getConstraintViolations().stream().map(ConstraintViolation::getMessage).collect(Collectors.joining());
//        return new CommonResult<String>().fail(message);
//    }
//
//    /**
//     * 处理缺少参数的请求数据,偶现BUG.在param中request=true出现  警惕:不要将HttpServletRequest传递到任何异步方法中
//     */
//
//
//    @ResponseStatus(HttpStatus.BAD_REQUEST)
//    @ExceptionHandler(MissingServletRequestParameterException.class)
//    public CommonResult<String> handleMissingServletRequestParameterException(MissingServletRequestParameterException e) {
//        log.error("处理缺少参数的请求数据", e);
//        return new CommonResult<String>().fail("required_parameter_is_not_present");
//    }
//
//    /**
//     * 处理请求数据时,没有对参数进行Json序列化
//     */
//    @ResponseStatus(HttpStatus.BAD_REQUEST)
//    @ExceptionHandler(HttpMessageNotReadableException.class)
//    public CommonResult<String> handleHttpMessageNotReadableException(HttpMessageNotReadableException e) {
//        log.error("参数解析失败,没有对参数进行Json序列化", e);
//        return new CommonResult<String>().fail("could_not_read_json");
//    }
//
//    /**
//     * 请求Dto,错误的属性配置 如@Size(min = 10, max = 6, message = "验证码长度应该在4-6位之间")
//     */
//    @ResponseStatus(HttpStatus.BAD_REQUEST)
//    @ExceptionHandler(ValidationException.class)
//    public CommonResult<String> handleValidationException(ValidationException e) {
//        log.error("参数验证失败", e);
//        return new CommonResult<String>().fail("validation_exception");
//    }
//
//    /**
//     * 请求方法错误
//     */
//    @ResponseStatus(HttpStatus.METHOD_NOT_ALLOWED)
//    @ExceptionHandler(HttpRequestMethodNotSupportedException.class)
//    public CommonResult<String> handleHttpRequestMethodNotSupportedException(HttpRequestMethodNotSupportedException e) {
//        log.error("不支持当前请求方法", e);
//        return new CommonResult<String>().fail("request_method_not_supported");
//    }
//
//    /**
//     * 415 - Unsupported Media Type 缺少"application/json;charset=UTF-8"类型的数据
//     */
//    @ResponseStatus(HttpStatus.UNSUPPORTED_MEDIA_TYPE)
//    @ExceptionHandler(HttpMediaTypeNotSupportedException.class)
//    public CommonResult<String> handleHttpMediaTypeNotSupportedException(Exception e) {
//        log.error("不支持当前媒体类型", e);
//        return new CommonResult<String>().fail("content_type_not_supported");
//    }
//
//}
