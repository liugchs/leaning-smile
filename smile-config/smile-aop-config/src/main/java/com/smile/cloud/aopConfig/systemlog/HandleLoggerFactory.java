/*
package com.smile.cloud.aopConfig.systemlog;


import com.starsand.common.result.CommonResult;

import java.util.Date;

*/
/**
 * @author xunyong
 * @date 2021/7/22 15:12
 * @Description: 系统日志 处理类工厂
 * @copyright 2021 mofang. All rights reserved
 *//*

public class HandleLoggerFactory {


    public static AbstractHandlerLogger initLoggerHandler(String methName, Object response, SystemLog systemLog, Date startTime, Date endTime,
                                                          Object[] methParams) {

        if (response instanceof CommonResult) {
            return new ControllerLogHandler(methName,response, systemLog, startTime, endTime, methParams);
        } else {
            //service 层日志处理
            return new ServiceLogHandler(methName,response, systemLog, startTime, endTime, methParams);
        }

    }

}
*/
