package com.smile.cloud.aopConfig.systemlog;


import java.util.Date;

/**
 * @author Xy.wang
 * @Description systemLog 系统日志处理Abstract类
 * @module star-sand
 * @date 2021/7/22 11:42
 * @copyright 2021 mofang. All rights reserved
 */
public abstract class AbstractHandlerLogger {

    /**
     * 返回信息
     */
    Object response;
    /**
     * 注解信息
     */
    SystemLog systemLog;

    /**
     * 开始时间
     */
    Date startTime;

    /**
     * 结束时间
     */
    Date endTime;

    /**
     * 入参
     */
    Object[] methodParams;

    /**
     * 方法名称
     */
    String methodName;

    public AbstractHandlerLogger(String methodName, Object response, SystemLog systemLog, Date startTime, Date endTime,
                                 Object[] methodParams) {
        this.methodName = methodName;
        this.response = response;
        this.systemLog = systemLog;
        this.startTime = startTime;
        this.endTime = endTime;
        this.methodParams = methodParams;
    }

    /**
     * Description: 输出日志
     *
     * @date 2020/4/15 15:10
     **/
    public abstract void outPutLogger();


}
