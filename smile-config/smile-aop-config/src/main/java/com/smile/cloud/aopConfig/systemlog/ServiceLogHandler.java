package com.smile.cloud.aopConfig.systemlog;


import cn.hutool.core.date.DatePattern;
import cn.hutool.core.date.DateUnit;
import cn.hutool.core.date.DateUtil;
import cn.hutool.json.JSONUtil;
import lombok.extern.slf4j.Slf4j;

import java.util.Arrays;
import java.util.Date;

/**
 * @author xunyong
 * @date 2021/7/22 15:12
 * @Description: service层系统日志处理
 * @copyright 2021 mofang. All rights reserved
 */
@Slf4j
public class ServiceLogHandler extends AbstractHandlerLogger {

    public ServiceLogHandler(String methName, Object response, SystemLog systemLog, Date startTime, Date endTime, Object[] methodParams) {
        super(methName, response, systemLog, startTime, endTime, methodParams);
    }

    @Override
    public void outPutLogger() {
        StringBuilder sb = new StringBuilder();
        //方法日志
        sb.append("\n");
        sb.append("<**************************************************************>").append("\n");
        sb.append("[method name ]:").append(methodName).append("\n");
        if (systemLog.show_req_body()) {
            sb.append("[method params]:").append(Arrays.toString(methodParams)).append("\n");
        } else {
            sb.append("[method params]:").append("not show").append("\n");
        }
        if (systemLog.show_resp_body()) {
            sb.append("[method return]:").append(JSONUtil.toJsonStr(response));
        } else {
            sb.append("[method return]:").append("not show").append("\n");
        }
        //处理返回参数
        sb.append("[begin time]:").append(DateUtil.format(startTime, DatePattern.CHINESE_DATE_TIME_PATTERN)).append("<<<<<<<<>>>>>>>>>>");
        sb.append("[end time]:").append(DateUtil.format(endTime, DatePattern.CHINESE_DATE_TIME_PATTERN)).append("<<<<<<<<>>>>>>>>>>");
        sb.append("[consume time]:").append(DateUtil.between(startTime, endTime, DateUnit.SECOND)).append("\n");
        sb.append("<**************************************************************>").append("\n");
        log.info(sb.toString());
    }
}
