/*
package com.smile.cloud.aopConfig.systemlog;


import cn.hutool.core.date.DatePattern;
import cn.hutool.core.date.DateUnit;
import cn.hutool.core.date.DateUtil;
import cn.hutool.json.JSONUtil;

import com.smile.cloud.util.LocalHttpUtil;
import lombok.extern.slf4j.Slf4j;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.Date;

*/
/**
 * @author xunyong
 * @date 2021/7/22 15:12
 * @Description: controller层系统日志处理
 * @copyright 2021 mofang. All rights reserved
 *//*

@Slf4j
public class ControllerLogHandler extends AbstractHandlerLogger {

    public ControllerLogHandler(String methodName, Object response, SystemLog systemLog, Date startTime, Date endTime,
                                Object[] methodParams) {
        super(methodName, response, systemLog, startTime, endTime, methodParams);
    }

    @Override
    public void outPutLogger() {
        StringBuilder sb = new StringBuilder();
        HttpServletRequest request = LocalHttpUtil.getRequest();
        // 处理请求参数
        sb.append("\n");
        sb.append("<===================================================>").append("\n");
        sb.append("[request Url ]:").append(request.getRequestURL()).append("\n");
        sb.append("[req header]:").append(request.getParameter("token")).append("\n");
        if (systemLog.show_req_body()) {
            sb.append("[req  body ]:").append(Arrays.toString(methodParams)).append("\n");
        } else {
            sb.append("	[req body]:").append("not show").append("\n");
        }
        //处理返回参数
        CommonResult result = (CommonResult) response;
        sb.append("[response message]:").append(result.getMsg()).append("\n");
        if (systemLog.show_resp_body()) {
            sb.append("[resp body]: ").append(JSONUtil.toJsonStr(result.getCode())).append("\n");
        } else {
            sb.append("[resp body]: null").append("\n");
        }
        sb.append("[request time]:").append(DateUtil.format(startTime, DatePattern.CHINESE_DATE_TIME_PATTERN)).append("<<<<<<<<>>>>>>>>>>");
        sb.append("[response time]:").append(DateUtil.format(endTime, DatePattern.CHINESE_DATE_TIME_PATTERN)).append("<<<<<<<<>>>>>>>>>>");
        sb.append("	[consume time]:").append(DateUtil.between(startTime, endTime, DateUnit.SECOND)).append("\n");
        sb.append("<===================================================>").append("\n");
        //输出日志
        if (result.getCode() >= ResultEnum.RS.getValue()) {
            log.info(sb.toString());
        } else {
            log.error(sb.toString());
        }
    }

}
*/
