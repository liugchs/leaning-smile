/*
package com.smile.cloud.aopConfig.systemlog;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

import java.util.Date;


*/
/**
 * @author Xy.wang
 * @Description systemLog 系统日志处理切面
 * @module star-sand
 * @date 2021/7/22 11:42
 * @copyright 2021 mofang. All rights reserved
 *//*

@Aspect
@Slf4j
@Component
public class ControllerLogAspect {

    @Around(value = "@annotation(systemLog)")
    public Object doAroundMethod(ProceedingJoinPoint pjd, SystemLog systemLog) throws Throwable {
        //方法名称
        String methName = pjd.getSignature().toShortString();
        // 开始时间
        Date startTime = new Date();
        // 执行方法
        Object respDto = pjd.proceed();
        // 结束时间
        Date endTime = new Date();
        //获取日志处理
        AbstractHandlerLogger logger = HandleLoggerFactory.initLoggerHandler(methName,
                                                                             respDto,
                                                                             systemLog,
                                                                             startTime,
                                                                             endTime,
                                                                             pjd.getArgs());
        //输出日志
        logger.outPutLogger();
        //获取返回类
        return respDto;
    }

}
*/
