package com.smile.cloud.aopConfig.systemlog;

import java.lang.annotation.*;

/**
 * @author xunyong
 * @date 2021/7/22 15:12
 * @Description: 系统日志切点
 * @copyright 2021 mofang. All rights reserved
 */
@Target({ElementType.PARAMETER, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
public @interface SystemLog {


    boolean value() default true;

    /**
     * Description: 日志输入，是否输出response body内容， 对于body内容很大的，没必要输出，可设置false
     *
     * @return boolean true
     * @date 2020/3/20 14:43
     **/
    boolean show_resp_body() default true;

    /**
     * Description: 日志输入,是否输出request 入参内容.入参过多不建议开启
     *
     * @return boolean
     * @date 2020/3/20 14:46
     **/
    boolean show_req_body() default true;

}
