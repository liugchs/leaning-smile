
package com.smile.cloud.ossConfig.oss.exception;

public class OssException extends ServiceException {
    private static final long serialVersionUID = -1979779664334663173L;
    private String resourceType;
    private String header;
    private String method;

    public OssException() {
    }

    public OssException(final String errorMessage) {
        super(errorMessage);
    }

    public OssException(final String errorMessage, final Throwable cause) {
        super(errorMessage, cause);
    }

    public OssException(final String errorMessage, final String errorCode, final String requestId, final String hostId, final String header, final String resourceType, final String method) {
        this(errorMessage, errorCode, requestId, hostId, header, resourceType, method, null, null);
    }

    public OssException(final String errorMessage, final String errorCode, final String requestId, final String hostId, final String header, final String resourceType, final String method, final Throwable cause) {
        this(errorMessage, errorCode, requestId, hostId, header, resourceType, method, null, cause);
    }

    public OssException(final String errorMessage, final String errorCode, final String requestId, final String hostId, final String header, final String resourceType, final String method, final String rawResponseError) {
        this(errorMessage, errorCode, requestId, hostId, header, resourceType, method, rawResponseError, null);
    }

    public OssException(final String errorMessage, final String errorCode, final String requestId, final String hostId, final String header, final String resourceType, final String method, final String rawResponseError, final Throwable cause) {
        super(errorMessage, errorCode, requestId, hostId, rawResponseError, cause);
        this.resourceType = resourceType;
        this.header = header;
        this.method = method;
    }

    public String getResourceType() {
        return this.resourceType;
    }

    public String getHeader() {
        return this.header;
    }

    public String getMethod() {
        return this.method;
    }

    @Override
    public String getMessage() {
        return super.getMessage() + ((this.resourceType == null) ? "" : ("\n[ResourceType]: " + this.resourceType)) + ((this.header == null) ? "" : ("\n[Header]: " + this.header)) + ((this.method == null) ? "" : ("\n[Method]: " + this.method));
    }
}
