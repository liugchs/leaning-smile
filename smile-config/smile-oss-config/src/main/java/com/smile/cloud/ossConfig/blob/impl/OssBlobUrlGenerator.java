
package com.smile.cloud.ossConfig.blob.impl;


import com.aliyun.oss.HttpMethod;

import com.smile.cloud.ossConfig.blob.ContentTypeCategory;
import com.smile.cloud.ossConfig.blob.BlobUrlGenerator;
import com.smile.cloud.ossConfig.blob.ContentTypeHelper;
import com.smile.cloud.ossConfig.oss.OssClient;
import com.smile.cloud.ossConfig.oss.model.OssObjectListResult;
import com.smile.cloud.ossConfig.oss.model.OssObjectSummary;
import com.smile.cloud.ossConfig.oss.model.OssPresignedUrlRequest;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component
public class OssBlobUrlGenerator implements BlobUrlGenerator {
    @Value("${aliyun.oss.bucketName}")
    private String storageBucketName;
    @Autowired
    protected OssClient ossClient;

    @Override
    public String url(final String filePath) throws Exception {
        final OssPresignedUrlRequest request = new OssPresignedUrlRequest(this.storageBucketName, filePath);
        request.setMethod(HttpMethod.GET);
        request.setExpiration(DateUtils.addHours(new Date(),12));
        final URL uri = this.ossClient.generatePresignedUrl(request);
        String url = uri.toString().replace("http://", "https://");
        return url;
    }

    @Override
    public String thumbnailUrl(final String path, final String contentType) throws Exception {
        return this.thumbnailUrl(path, contentType, 480, 320);
    }

    @Override
    public String thumbnailUrl(final String path, final String contentType, final int width, final int height) throws Exception {
        String process = "";
        final ContentTypeCategory contentTypeCategory = ContentTypeHelper.GetCategory(contentType);
        switch (contentTypeCategory) {
            case Image: {
                process = "image/resize,w_" + width + ",h_" + height + ",color_FFFFFF";
                break;
            }
            case Video: {
                process = "video/snapshot,t_0,f_jpg,w_480,h_320";
                break;
            }
        }
        if (!StringUtils.isEmpty((Object) path)) {
            final OssPresignedUrlRequest request = new OssPresignedUrlRequest(this.storageBucketName, path);
            request.setMethod(HttpMethod.GET);
            request.setExpiration(DateUtils.addHours(new Date(), 1));
            request.setProcess(process);
            final URL uri = this.ossClient.generatePresignedUrl(request);
            return uri.toString();
        }
        return "";
    }

    @Override
    public List<String> listUrl(final String dir) throws Exception {
        final OssObjectListResult result = this.ossClient.listObjects(this.storageBucketName, dir);
        final List<OssObjectSummary> objectSummaries = (List<OssObjectSummary>) result.getObjectSummaries();
        final List<String> listUrl = new ArrayList<String>();
        final String[] key = new String[1];
        final String[] url = new String[1];
        final List<String> list = null;
        objectSummaries.forEach(o -> {
            key[0] = o.getKey();
            try {
                url[0] = this.url(key[0]);
                list.add(url[0]);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return;
        });
        return listUrl;
    }

    @Override
    public List<String> listKey(final String dir) throws Exception {
        final OssObjectListResult result = this.ossClient.listObjects(this.storageBucketName, dir);
        final List<OssObjectSummary> objectSummaries = (List<OssObjectSummary>) result.getObjectSummaries();
        final List<String> listKey = new ArrayList<String>();
        objectSummaries.forEach(o -> listKey.add(o.getKey()));
        return listKey;
    }
}
