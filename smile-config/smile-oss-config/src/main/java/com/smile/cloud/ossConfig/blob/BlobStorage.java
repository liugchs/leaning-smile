
package com.smile.cloud.ossConfig.blob;

import java.io.InputStream;

public interface BlobStorage {
    void create(final byte[] dataBytes, final String storeDir, final String fileName) throws Exception;

    void create(final InputStream fileStream, final String storeDir, final String fileName) throws Exception;

    byte[] fetch(final String filePath) throws Exception;

    void copy(final String srcFilePath, final String destFilePath) throws Exception;

    void move(final String srcFilePath, final String destFilePath) throws Exception;

    void delete(final String filePath) throws Exception;

    void deleteFolder(final String folderPath) throws Exception;
}
