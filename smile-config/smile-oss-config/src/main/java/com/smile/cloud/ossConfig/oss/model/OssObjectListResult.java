
package com.smile.cloud.ossConfig.oss.model;

import java.util.ArrayList;
import java.util.List;

public class OssObjectListResult {
    private List<OssObjectSummary> objectSummaries;
    private List<String> commonPrefixes;
    private String bucketName;
    private String nextMarker;
    private boolean isTruncated;
    private String prefix;
    private String marker;
    private int maxKeys;
    private String delimiter;
    private String encodingType;

    public OssObjectListResult() {
        this.objectSummaries = new ArrayList<OssObjectSummary>();
        this.commonPrefixes = new ArrayList<String>();
    }

    public List<OssObjectSummary> getObjectSummaries() {
        return this.objectSummaries;
    }

    public void addObjectSummary(final OssObjectSummary objectSummary) {
        this.objectSummaries.add(objectSummary);
    }

    public void setObjectSummaries(final List<OssObjectSummary> objectSummaries) {
        this.objectSummaries.clear();
        if (objectSummaries != null && !objectSummaries.isEmpty()) {
            this.objectSummaries.addAll(objectSummaries);
        }
    }

    public void clearObjectSummaries() {
        this.objectSummaries.clear();
    }

    public List<String> getCommonPrefixes() {
        return this.commonPrefixes;
    }

    public void addCommonPrefix(final String commonPrefix) {
        this.commonPrefixes.add(commonPrefix);
    }

    public void setCommonPrefixes(final List<String> commonPrefixes) {
        this.commonPrefixes.clear();
        if (commonPrefixes != null && !commonPrefixes.isEmpty()) {
            this.commonPrefixes.addAll(commonPrefixes);
        }
    }

    public void clearCommonPrefixes() {
        this.commonPrefixes.clear();
    }

    public String getNextMarker() {
        return this.nextMarker;
    }

    public void setNextMarker(final String nextMarker) {
        this.nextMarker = nextMarker;
    }

    public String getBucketName() {
        return this.bucketName;
    }

    public void setBucketName(final String bucketName) {
        this.bucketName = bucketName;
    }

    public String getPrefix() {
        return this.prefix;
    }

    public void setPrefix(final String prefix) {
        this.prefix = prefix;
    }

    public String getMarker() {
        return this.marker;
    }

    public void setMarker(final String marker) {
        this.marker = marker;
    }

    public int getMaxKeys() {
        return this.maxKeys;
    }

    public void setMaxKeys(final int maxKeys) {
        this.maxKeys = maxKeys;
    }

    public String getDelimiter() {
        return this.delimiter;
    }

    public void setDelimiter(final String delimiter) {
        this.delimiter = delimiter;
    }

    public String getEncodingType() {
        return this.encodingType;
    }

    public void setEncodingType(final String encodingType) {
        this.encodingType = encodingType;
    }

    public boolean isTruncated() {
        return this.isTruncated;
    }

    public void setTruncated(final boolean isTruncated) {
        this.isTruncated = isTruncated;
    }
}
