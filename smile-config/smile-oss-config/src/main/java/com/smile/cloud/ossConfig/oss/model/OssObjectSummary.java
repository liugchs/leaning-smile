
package com.smile.cloud.ossConfig.oss.model;

import java.util.Date;

public class OssObjectSummary {
    private String bucketName;
    private String key;
    private String eTag;
    private long size;
    private Date lastModified;
    private String storageClass;
    private OssOwner owner;

    public String getBucketName() {
        return this.bucketName;
    }

    public void setBucketName(final String bucketName) {
        this.bucketName = bucketName;
    }

    public String getKey() {
        return this.key;
    }

    public void setKey(final String key) {
        this.key = key;
    }

    public String getETag() {
        return this.eTag;
    }

    public void setETag(final String eTag) {
        this.eTag = eTag;
    }

    public long getSize() {
        return this.size;
    }

    public void setSize(final long size) {
        this.size = size;
    }

    public Date getLastModified() {
        return this.lastModified;
    }

    public void setLastModified(final Date lastModified) {
        this.lastModified = lastModified;
    }

    public OssOwner getOwner() {
        return this.owner;
    }

    public void setOwner(final OssOwner owner) {
        this.owner = owner;
    }

    public String getStorageClass() {
        return this.storageClass;
    }

    public void setStorageClass(final String storageClass) {
        this.storageClass = storageClass;
    }
}
