
package com.smile.cloud.ossConfig.oss.model;


import com.aliyun.oss.HttpMethod;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class OssPresignedUrlRequest {
    private HttpMethod method;
    private String bucketName;
    private String key;
    private String contentType;
    private String contentMD5;
    private String process;
    private Date expiration;
    private Map<String, String> userMetadata;
    private Map<String, String> queryParam;

    public OssPresignedUrlRequest(final String bucketName, final String key) {
        this(bucketName, key, HttpMethod.GET);
    }

    public OssPresignedUrlRequest(final String bucketName, final String key, final HttpMethod method) {
        this.userMetadata = new HashMap<String, String>();
        this.queryParam = new HashMap<String, String>();
        this.bucketName = bucketName;
        this.key = key;
        this.method = method;
    }

    public HttpMethod getMethod() {
        return this.method;
    }

    public void setMethod(final HttpMethod method) {
        if (method != HttpMethod.GET && method != HttpMethod.PUT) {
            throw new IllegalArgumentException("\u4ec5\u652f\u6301GET\u548cPUT\u65b9\u6cd5\u3002");
        }
        this.method = method;
    }

    public String getBucketName() {
        return this.bucketName;
    }

    public void setBucketName(final String bucketName) {
        this.bucketName = bucketName;
    }

    public String getKey() {
        return this.key;
    }

    public void setKey(final String key) {
        this.key = key;
    }

    public Date getExpiration() {
        return this.expiration;
    }

    public void setExpiration(final Date expiration) {
        this.expiration = expiration;
    }

    public void setContentType(final String contentType) {
        this.contentType = contentType;
    }

    public String getContentType() {
        return this.contentType;
    }

    public void setContentMD5(final String contentMD5) {
        this.contentMD5 = contentMD5;
    }

    public String getContentMD5() {
        return this.contentMD5;
    }

    public Map<String, String> getUserMetadata() {
        return this.userMetadata;
    }

    public void setUserMetadata(final Map<String, String> userMetadata) {
        if (userMetadata == null) {
            throw new NullPointerException("\u53c2\u6570'userMeta'\u4e3a\u7a7a\u6307\u9488\u3002");
        }
        this.userMetadata = userMetadata;
    }

    public void addUserMetadata(final String key, final String value) {
        this.userMetadata.put(key, value);
    }

    public Map<String, String> getQueryParameter() {
        return this.queryParam;
    }

    public void setQueryParameter(final Map<String, String> queryParam) {
        if (queryParam == null) {
            throw new NullPointerException("\u53c2\u6570'queryParameter'\u4e3a\u7a7a\u6307\u9488\u3002");
        }
        this.queryParam = queryParam;
    }

    public void addQueryParameter(final String key, final String value) {
        this.queryParam.put(key, value);
    }

    public String getProcess() {
        return this.process;
    }

    public void setProcess(final String process) {
        this.process = process;
    }
}
