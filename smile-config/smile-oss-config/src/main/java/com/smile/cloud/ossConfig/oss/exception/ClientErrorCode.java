
package com.smile.cloud.ossConfig.oss.exception;

public interface ClientErrorCode {
    public static final String UNKNOWN = "Unknown";
    public static final String UNKNOWN_HOST = "UnknownHost";
    public static final String CONNECTION_TIMEOUT = "ConnectionTimeout";
    public static final String SOCKET_TIMEOUT = "SocketTimeout";
    public static final String SOCKET_EXCEPTION = "SocketException";
    public static final String CONNECTION_REFUSED = "ConnectionRefused";
    public static final String NONREPEATABLE_REQUEST = "NonRepeatableRequest";
}
