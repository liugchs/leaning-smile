
package com.smile.cloud.ossConfig.oss.model;

public class Credentials {
    String accessKeyId;
    String accessKeySecret;
    String securityToken;
    boolean useToken;

    public Credentials(final String accessKeyId, final String accessKeySecret, final String securityToken, final boolean useToken) {
        this.accessKeyId = accessKeyId;
        this.accessKeySecret = accessKeySecret;
        this.securityToken = securityToken;
        this.useToken = useToken;
    }

    public String getAccessKeyId() {
        return this.accessKeyId;
    }

    public String getAccessKeySecret() {
        return this.accessKeySecret;
    }

    public String getSecurityToken() {
        return this.securityToken;
    }

    public boolean isUseToken() {
        return this.useToken;
    }
}
