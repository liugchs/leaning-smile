
package com.smile.cloud.ossConfig.oss.model;

import com.aliyun.oss.common.utils.DateUtil;

import java.text.ParseException;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class OssMetadata {
    private static final String LAST_MODIFIED = "Last-Modified";
    private static final String EXPIRES = "Expires";
    private static final String CONTENT_LENGTH = "Content-Length";
    private static final String CONTENT_TYPE = "Content-Type";
    private static final String CONTENT_MD5 = "Content-MD5";
    private static final String CONTENT_ENCODING = "Content-Encoding";
    private static final String CACHE_CONTROL = "Cache-Control";
    private static final String CONTENT_DISPOSITION = "Content-Disposition";
    private static final String ETAG = "ETag";
    private static final String OSS_SERVER_SIDE_ENCRYPTION = "x-oss-server-side-encryption";
    private static final String OSS_OBJECT_TYPE = "x-oss-object-type";
    private static final String OSS_OBJECT_ACL = "x-oss-object-acl";
    private static final String OSS_HEADER_REQUEST_ID = "x-oss-request-id";
    private Map<String, String> userMetadata;
    private Map<String, Object> metadata;
    public static final String AES_256_SERVER_SIDE_ENCRYPTION = "AES256";

    public OssMetadata() {
        this.userMetadata = new HashMap<String, String>();
        this.metadata = new HashMap<String, Object>();
    }

    public Map<String, String> getUserMetadata() {
        return this.userMetadata;
    }

    public void setUserMetadata(final Map<String, String> userMetadata) {
        this.userMetadata.clear();
        if (userMetadata != null && !userMetadata.isEmpty()) {
            this.userMetadata.putAll(userMetadata);
        }
    }

    public void setHeader(final String key, final Object value) {
        this.metadata.put(key, value);
    }

    public void addUserMetadata(final String key, final String value) {
        this.userMetadata.put(key, value);
    }

    public Date getLastModified() {
        return (Date) this.metadata.get("Last-Modified");
    }

    public void setLastModified(final Date lastModified) {
        this.metadata.put("Last-Modified", lastModified);
    }

    public Date getExpirationTime() throws ParseException {
        return DateUtil.parseRfc822Date((String) this.metadata.get("Expires"));
    }

    public String getRawExpiresValue() {
        return (String) this.metadata.get("Expires");
    }

    public void setExpirationTime(final Date expirationTime) {
        this.metadata.put("Expires", DateUtil.formatRfc822Date(expirationTime));
    }

    public long getContentLength() {
        final Long contentLength = (Long) this.metadata.get("Content-Length");
        return (contentLength == null) ? 0L : contentLength;
    }

    public void setContentLength(final long contentLength) {
        this.metadata.put("Content-Length", contentLength);
    }

    public String getContentType() {
        return (String) this.metadata.get("Content-Type");
    }

    public void setContentType(final String contentType) {
        this.metadata.put("Content-Type", contentType);
    }

    public String getContentMD5() {
        return (String) this.metadata.get("Content-MD5");
    }

    public void setContentMD5(final String contentMD5) {
        this.metadata.put("Content-MD5", contentMD5);
    }

    public String getContentEncoding() {
        return (String) this.metadata.get("Content-Encoding");
    }

    public void setContentEncoding(final String encoding) {
        this.metadata.put("Content-Encoding", encoding);
    }

    public String getCacheControl() {
        return (String) this.metadata.get("Cache-Control");
    }

    public void setCacheControl(final String cacheControl) {
        this.metadata.put("Cache-Control", cacheControl);
    }

    public String getContentDisposition() {
        return (String) this.metadata.get("Content-Disposition");
    }

    public void setContentDisposition(final String disposition) {
        this.metadata.put("Content-Disposition", disposition);
    }

    public String getETag() {
        return (String) this.metadata.get("ETag");
    }

    public String getServerSideEncryption() {
        return (String) this.metadata.get("x-oss-server-side-encryption");
    }

    public void setServerSideEncryption(final String serverSideEncryption) {
        this.metadata.put("x-oss-server-side-encryption", serverSideEncryption);
    }

    public String getObjectType() {
        return (String) this.metadata.get("x-oss-object-type");
    }

    public void setObjectAcl(final AccessControlList cannedAcl) {
        this.metadata.put("x-oss-object-acl", (cannedAcl != null) ? cannedAcl.toString() : "");
    }

    public Map<String, Object> getRawMetadata() {
        return Collections.unmodifiableMap((Map<? extends String, ?>) this.metadata);
    }

    public void setMetadata(final Map<String, Object> metadata) {
        this.metadata.clear();
        if (metadata != null && !metadata.isEmpty()) {
            this.metadata.putAll(metadata);
        }
    }

    public String getRequestId() {
        return (String) this.metadata.get("x-oss-request-id");
    }
}
