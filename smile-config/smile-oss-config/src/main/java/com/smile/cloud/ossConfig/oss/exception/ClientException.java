
package com.smile.cloud.ossConfig.oss.exception;

public class ClientException extends RuntimeException {
    private static final long serialVersionUID = 1870835486798448798L;
    private String errorMessage;
    private String requestId;
    private String errorCode;

    public ClientException() {
    }

    public ClientException(final String errorMessage) {
        this(errorMessage, null);
    }

    public ClientException(final Throwable cause) {
        this(null, cause);
    }

    public ClientException(final String errorMessage, final Throwable cause) {
        super(null, cause);
        this.errorMessage = errorMessage;
        this.errorCode = "Unknown";
        this.requestId = "Unknown";
    }

    public ClientException(final String errorMessage, final String errorCode, final String requestId) {
        this(errorMessage, errorCode, requestId, null);
    }

    public ClientException(final String errorMessage, final String errorCode, final String requestId, final Throwable cause) {
        this(errorMessage, cause);
        this.errorCode = errorCode;
        this.requestId = requestId;
    }

    public String getErrorMessage() {
        return this.errorMessage;
    }

    public String getErrorCode() {
        return this.errorCode;
    }

    public String getRequestId() {
        return this.requestId;
    }

    @Override
    public String getMessage() {
        return (this.getErrorMessage() + "\n[ErrorCode]: " + this.errorCode != null) ? this.errorCode : (("\n[RequestId]: " + this.requestId != null) ? this.requestId : "");
    }
}
