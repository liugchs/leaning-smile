

package com.smile.cloud.ossConfig;


import java.util.UUID;

public final class FileUtils {
    public static String getExtension(final String filename) {
        if (filename == null) {
            return null;
        }
        final int extensionPos = filename.lastIndexOf(".");
        final int lastUnixPos = filename.lastIndexOf("/");
        final int lastWindowsPos = filename.lastIndexOf("\\");
        final int lastSeparator = Math.max(lastUnixPos, lastWindowsPos);
        final int index = (lastSeparator > extensionPos) ? -1 : extensionPos;
        if (index == -1) {
            return "";
        }
        return filename.substring(index + 1);
    }

    public static String concat(final String... fileNameParts) {
        String res = "";
        for (final String part : fileNameParts) {
            if (!StringUtil.isEmpty((CharSequence) res)) {
                res = StringUtil.ensureEndsWith(res, "/");
            }
            res += part;
        }
        return res;
    }

    public static String createRadomFileName(final String extension) {
        String tempFileName = UUID.randomUUID().toString().replaceAll("-", "");
        if (!StringUtil.isEmpty((CharSequence) extension)) {
            tempFileName = tempFileName + "." + extension;
        }
        return tempFileName;
    }

    public static String getContentType(final String filenameExtension) {
        if (filenameExtension.equalsIgnoreCase("bmp")) {
            return "image/bmp";
        }
        if (filenameExtension.equalsIgnoreCase("gif")) {
            return "image/gif";
        }
        if (filenameExtension.equalsIgnoreCase("jpeg") || filenameExtension.equalsIgnoreCase("jpg") || filenameExtension.equalsIgnoreCase("png")) {
            return "image/jpg";
        }
        if (filenameExtension.equalsIgnoreCase("html")) {
            return "text/html";
        }
        if (filenameExtension.equalsIgnoreCase("txt")) {
            return "text/plain";
        }
        if (filenameExtension.equalsIgnoreCase("vsd")) {
            return "application/vnd.visio";
        }
        if (filenameExtension.equalsIgnoreCase("pptx") || filenameExtension.equalsIgnoreCase("ppt")) {
            return "application/vnd.ms-powerpoint";
        }
        if (filenameExtension.equalsIgnoreCase("docx") || filenameExtension.equalsIgnoreCase("doc")) {
            return "application/msword";
        }
        if (filenameExtension.equalsIgnoreCase("xml")) {
            return "text/xml";
        }
        if (filenameExtension.equalsIgnoreCase("pdf")) {
            return "application/pdf";
        }
        return "";
    }
}
