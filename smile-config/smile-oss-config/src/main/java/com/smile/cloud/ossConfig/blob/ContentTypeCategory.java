
package com.smile.cloud.ossConfig.blob;

public enum ContentTypeCategory {
    Image,
    Video,
    Text,
    Word,
    Excel,
    PowerPoint,
    Pdf,
    Other;
}
