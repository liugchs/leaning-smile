
package com.smile.cloud.ossConfig.oss.model;

import java.io.Serializable;

public class OssOwner implements Serializable {
    private static final long serialVersionUID = -1942759024112448066L;
    private String displayName;
    private String id;

    public OssOwner() {
    }

    public OssOwner(final String id, final String displayName) {
        this.id = id;
        this.displayName = displayName;
    }

    @Override
    public String toString() {
        return "Owner [name=" + this.getDisplayName() + ",id=" + this.getId() + "]";
    }

    public String getId() {
        return this.id;
    }

    public void setId(final String id) {
        this.id = id;
    }

    public String getDisplayName() {
        return this.displayName;
    }

    public void setDisplayName(final String name) {
        this.displayName = name;
    }

    @Override
    public boolean equals(final Object obj) {
        if (!(obj instanceof OssOwner)) {
            return false;
        }
        final OssOwner otherOwner = (OssOwner) obj;
        String otherOwnerId = otherOwner.getId();
        String otherOwnerName = otherOwner.getDisplayName();
        String thisOwnerId = this.getId();
        String thisOwnerName = this.getDisplayName();
        if (otherOwnerId == null) {
            otherOwnerId = "";
        }
        if (otherOwnerName == null) {
            otherOwnerName = "";
        }
        if (thisOwnerId == null) {
            thisOwnerId = "";
        }
        if (thisOwnerName == null) {
            thisOwnerName = "";
        }
        return otherOwnerId.equals(thisOwnerId) && otherOwnerName.equals(thisOwnerName);
    }

    @Override
    public int hashCode() {
        if (this.id != null) {
            return this.id.hashCode();
        }
        return 0;
    }
}
