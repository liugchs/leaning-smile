
package com.smile.cloud.ossConfig.oss.model;

import java.io.InputStream;

public class OssStreamResponse extends OssGenericResult {
    private InputStream callbackResponseBody;

    public InputStream getCallbackResponseBody() {
        return this.callbackResponseBody;
    }

    public void setCallbackResponseBody(final InputStream callbackResponseBody) {
        this.callbackResponseBody = callbackResponseBody;
    }

    public OssStreamResponse(final InputStream callbackResponseBody, final String requestId, final Long clientCRC, final Long serverCRC) {
        this.callbackResponseBody = callbackResponseBody;
        this.setRequestId(this.getRequestId());
        this.setClientCRC(this.getClientCRC());
        this.setServerCRC(this.getServerCRC());
    }
}
