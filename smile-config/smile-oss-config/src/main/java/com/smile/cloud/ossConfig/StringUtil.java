
package com.smile.cloud.ossConfig;

import org.apache.commons.lang3.StringUtils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

public final class StringUtil extends StringUtils
{
    public static String noPunctNoSpace(final String str) {
        return str.replaceAll("[\\p{Punct}\\p{Space}]+", "");
    }
    
    public static String ensureEndsWith(String str, final String suffix) {
        if (!endsWith((CharSequence)str, (CharSequence)suffix)) {
            str += suffix;
        }
        return str;
    }
    
    public static String getSubStr(String str, final int num) {
        String result = "";
        for (int i = 0; i < num; ++i) {
            final int lastFirst = str.lastIndexOf(47);
            result = str.substring(lastFirst) + result;
            str = str.substring(0, lastFirst);
        }
        return result.substring(1);
    }
    
    public static String buildId(String preStr) {
        if (StringUtils.isEmpty((CharSequence)preStr)) {
            preStr = "";
        }
        final String uuid = UUID.randomUUID().toString().replaceAll("-", "").toUpperCase();
        final SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        final String time = sdf.format(new Date(System.currentTimeMillis()));
        return preStr + time + uuid;
    }
    
    public static String getUUID() {
        return UUID.randomUUID().toString().replaceAll("-", "");
    }

}
