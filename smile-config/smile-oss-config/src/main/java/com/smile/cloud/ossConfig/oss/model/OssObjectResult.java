
package com.smile.cloud.ossConfig.oss.model;

import java.io.InputStream;

public class OssObjectResult extends OssGenericResult {
    private String key;
    private String bucketName;
    private OssMetadata metadata;
    private InputStream content;

    public String getKey() {
        return this.key;
    }

    public void setKey(final String key) {
        this.key = key;
    }

    public String getBucketName() {
        return this.bucketName;
    }

    public void setBucketName(final String bucketName) {
        this.bucketName = bucketName;
    }

    public OssMetadata getMetadata() {
        return this.metadata;
    }

    public void setMetadata(final OssMetadata metadata) {
        this.metadata = metadata;
    }

    public InputStream getContent() {
        return this.content;
    }

    public void setContent(final InputStream content) {
        this.content = content;
    }
}
