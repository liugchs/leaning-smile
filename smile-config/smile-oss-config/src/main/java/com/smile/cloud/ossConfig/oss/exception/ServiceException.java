// 
// Decompiled by Procyon v0.5.36
// 

package com.smile.cloud.ossConfig.oss.exception;

public class ServiceException extends RuntimeException
{
    private static final long serialVersionUID = 430933593095358673L;
    private String errorMessage;
    private String errorCode;
    private String requestId;
    private String hostId;
    private String rawResponseError;
    
    public ServiceException() {
    }
    
    public ServiceException(final String errorMessage) {
        super((String)null);
        this.errorMessage = errorMessage;
    }
    
    public ServiceException(final Throwable cause) {
        super(null, cause);
    }
    
    public ServiceException(final String errorMessage, final Throwable cause) {
        super(null, cause);
        this.errorMessage = errorMessage;
    }
    
    public ServiceException(final String errorMessage, final String errorCode, final String requestId, final String hostId) {
        this(errorMessage, errorCode, requestId, hostId, null);
    }
    
    public ServiceException(final String errorMessage, final String errorCode, final String requestId, final String hostId, final Throwable cause) {
        this(errorMessage, errorCode, requestId, hostId, null, cause);
    }
    
    public ServiceException(final String errorMessage, final String errorCode, final String requestId, final String hostId, final String rawResponseError, final Throwable cause) {
        this(errorMessage, cause);
        this.errorCode = errorCode;
        this.requestId = requestId;
        this.hostId = hostId;
        this.rawResponseError = rawResponseError;
    }
    
    public String getErrorMessage() {
        return this.errorMessage;
    }
    
    public String getErrorCode() {
        return this.errorCode;
    }
    
    public String getRequestId() {
        return this.requestId;
    }
    
    public String getHostId() {
        return this.hostId;
    }
    
    public String getRawResponseError() {
        return this.rawResponseError;
    }
    
    public void setRawResponseError(final String rawResponseError) {
        this.rawResponseError = rawResponseError;
    }
    
    private String formatRawResponseError() {
        if (this.rawResponseError == null || this.rawResponseError.equals("")) {
            return "";
        }
        return String.format("\n[ResponseError]:\n%s", this.rawResponseError);
    }
    
    @Override
    public String getMessage() {
        return this.getErrorMessage() + "\n[ErrorCode]: " + this.getErrorCode() + "\n[RequestId]: " + this.getRequestId() + "\n[HostId]: " + this.getHostId() + this.formatRawResponseError();
    }
}
