
package com.smile.cloud.ossConfig.oss.model;

import java.util.ArrayList;
import java.util.List;

public class OssDeleteObjectsResult extends OssGenericResult {
    private final List<String> deletedObjects;
    private String encodingType;

    public OssDeleteObjectsResult() {
        this.deletedObjects = new ArrayList<String>();
    }

    public OssDeleteObjectsResult(final List<String> deletedObjects) {
        this.deletedObjects = new ArrayList<String>();
        if (deletedObjects != null && deletedObjects.size() > 0) {
            this.deletedObjects.addAll(deletedObjects);
        }
    }

    public List<String> getDeletedObjects() {
        return this.deletedObjects;
    }

    public void setDeletedObjects(final List<String> deletedObjects) {
        this.deletedObjects.clear();
        this.deletedObjects.addAll(deletedObjects);
    }

    public String getEncodingType() {
        return this.encodingType;
    }

    public void setEncodingType(final String encodingType) {
        this.encodingType = encodingType;
    }
}
