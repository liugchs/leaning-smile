

package com.smile.cloud.ossConfig.blob;

import java.util.List;

public interface BlobUrlGenerator {
    String url(final String filePath) throws Exception;

    String thumbnailUrl(final String path, final String contentType) throws Exception;

    String thumbnailUrl(final String path, final String contentType, final int width, final int height) throws Exception;

    List<String> listUrl(final String dir) throws Exception;

    List<String> listKey(final String dir) throws Exception;
}
