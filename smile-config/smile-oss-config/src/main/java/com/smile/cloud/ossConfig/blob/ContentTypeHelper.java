
package com.smile.cloud.ossConfig.blob;

public class ContentTypeHelper {
    public static final String IMAGE_JPEG = "image/jpeg";
    public static final String IMAGE_JPG = "image/jpg";
    public static final String IMAGE_BMP = "image/bmp";
    public static final String IMAGE_PNG = "image/png";
    public static final String IMAGE_GIF = "image/gif";
    public static final String VIDEO_MPEG4 = "video/mpeg4";
    public static final String APPLICATION_PDF = "application/pdf";
    public static final String APPLICATION_DOC = "application/msword";
    public static final String APPLICATION_DOCX = "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
    public static final String APPLICATION_XLS = "application/vnd.ms-excel";
    public static final String APPLICATION_XLSX = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
    public static final String APPLICATION_PPT = "application/vnd.ms-powerpoint";
    public static final String APPLICATION_PPTX = "application/vnd.openxmlformats-officedocument.presentationml.presentation";
    public static final String TEXT = "text/plain";

    public static ContentTypeCategory GetCategory(final String contentType) {
        switch (contentType) {
            case "image/bmp":
            case "image/jpg":
            case "image/jpeg":
            case "image/png":
            case "image/gif": {
                return ContentTypeCategory.Image;
            }
            case "video/mpeg4": {
                return ContentTypeCategory.Video;
            }
            case "application/pdf": {
                return ContentTypeCategory.Pdf;
            }
            case "application/msword":
            case "application/vnd.openxmlformats-officedocument.wordprocessingml.document": {
                return ContentTypeCategory.Word;
            }
            case "application/vnd.ms-excel":
            case "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet": {
                return ContentTypeCategory.Excel;
            }
            case "application/vnd.ms-powerpoint":
            case "application/vnd.openxmlformats-officedocument.presentationml.presentation": {
                return ContentTypeCategory.PowerPoint;
            }
            case "text/plain": {
                return ContentTypeCategory.Text;
            }
            default: {
                return ContentTypeCategory.Other;
            }
        }
    }
}
