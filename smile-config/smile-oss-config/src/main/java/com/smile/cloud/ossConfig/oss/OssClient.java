
package com.smile.cloud.ossConfig.oss;


import com.aliyun.oss.model.ObjectMetadata;
import com.smile.cloud.ossConfig.oss.model.*;

import java.io.InputStream;
import java.net.URL;
import java.util.List;
import java.util.Map;

public interface OssClient {
    void shutdown();

    URL generatePresignedUrl(final OssPresignedUrlRequest request) throws Exception;

    OssObjectResult getObject(final String bucketName, final String key) throws Exception;

    OssStreamResponse putObject(final String bucketName, final String key, final InputStream stream) throws Exception;
    OssStreamResponse putObject(final String bucketName, final String key, final InputStream stream,final ObjectMetadata metadata) throws Exception;

    OssStreamResponse putObject(final String bucketName, final String key, final InputStream stream, final String callbackUrl, final String callbackBody, final Map<String, String> userVariables) throws Exception;

    void deleteObject(final String bucketName, final String key) throws Exception;

    void copyObject(final String srcBucketName, final String sourceKey, final String destinationBucketName, final String destinationKey) throws Exception;

    OssObjectListResult listObjects(final String bucketName, final String prefix) throws Exception;

    OssDeleteObjectsResult deleteObjects(final String bucketName, final List<String> keys) throws Exception;

    OssDeleteObjectsResult deleteObjects(final String bucketName, final List<String> keys, final boolean quiet) throws Exception;

    void setEndpoint(final String endPoint);

    void switchCredentials(final Credentials creds);
}
