
package com.smile.cloud.ossConfig.oss.model;

public abstract class OssGenericResult {
    private String requestId;
    private Long clientCRC;
    private Long serverCRC;

    public String getRequestId() {
        return this.requestId;
    }

    public void setRequestId(final String requestId) {
        this.requestId = requestId;
    }

    public Long getClientCRC() {
        return this.clientCRC;
    }

    public void setClientCRC(final Long clientCRC) {
        this.clientCRC = clientCRC;
    }

    public Long getServerCRC() {
        return this.serverCRC;
    }

    public void setServerCRC(final Long serverCRC) {
        this.serverCRC = serverCRC;
    }
}
