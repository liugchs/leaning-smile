package com.smile.cloud.ossConfig.oss.model;

public enum AccessControlList {
    Default("default"),
    Private("private"),
    PublicRead("public-read"),
    PublicReadWrite("public-read-write");

    private String aclString;

    private AccessControlList(final String aclString) {
        this.aclString = aclString;
    }

    @Override
    public String toString() {
        return this.aclString;
    }

    public static AccessControlList parse(final String acl) {
        for (final AccessControlList cacl : values()) {
            if (cacl.toString().equals(acl)) {
                return cacl;
            }
        }
        throw new IllegalArgumentException("Unable to parse the provided acl " + acl);
    }
}
