// 
// Decompiled by Procyon v0.5.36
// 

package com.smile.cloud.ossConfig.oss;

import com.aliyun.oss.OSSClient;
import com.aliyun.oss.model.CopyObjectResult;
import com.aliyun.oss.model.ObjectMetadata;
import com.aliyun.oss.model.PutObjectResult;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.io.*;
import java.net.URL;
import java.util.Date;

@Component
public class OSSClientUtil
{
    private static String endPoint;
    private static String accessKeyId;
    private static String accessKeySecret;
    private static String bucketName;
    private static String dir;
    private static OSSClient ossClient;
    
    public OSSClientUtil() {
        OSSClientUtil.ossClient = new OSSClient(OSSClientUtil.endPoint, OSSClientUtil.accessKeyId, OSSClientUtil.accessKeySecret);
    }
    
    public static void init() {
        OSSClientUtil.ossClient = new OSSClient(OSSClientUtil.endPoint, OSSClientUtil.accessKeyId, OSSClientUtil.accessKeySecret);
    }
    
    public static void destory() {
        OSSClientUtil.ossClient.shutdown();
    }
    
    public static void uploadToTemp(final String url) throws Exception {
        final File fileOnServer = new File(url);
        try {
            final FileInputStream fin = new FileInputStream(fileOnServer);
            final String[] split = url.split("/");
            uploadFile2OSS(fin, split[split.length - 1]);
        }
        catch (FileNotFoundException e) {
            throw new Exception("\u56fe\u7247\u4e0a\u4f20\u5931\u8d25");
        }
    }
    
    public static String uploadFile2OSS(final InputStream inputStream, final String fileName) throws IOException {
        String ret = "";
        try {
            final ObjectMetadata objectMetadata = new ObjectMetadata();
            objectMetadata.setContentLength((long)inputStream.available());
            objectMetadata.setCacheControl("no-cache");
            objectMetadata.setHeader("Pragma", (Object)"no-cache");
            objectMetadata.setContentType(getContentType(fileName.substring(fileName.lastIndexOf(".") + 1)));
            objectMetadata.setContentDisposition("inline;filename=" + fileName);
            final PutObjectResult putResult = OSSClientUtil.ossClient.putObject(OSSClientUtil.bucketName, OSSClientUtil.dir + fileName, inputStream, objectMetadata);
            ret = putResult.getETag();
        }
        catch (IOException e) {
            e.getMessage();
        }
        finally {
            inputStream.close();
        }
        return ret;
    }
    
    public static String getFileType(final String path) {
        if (!StringUtils.isEmpty((Object)path)) {
            final String filenameExtension = path.substring(path.lastIndexOf(".") + 1);
            return getContentType(filenameExtension);
        }
        return null;
    }
    
    public static String getContentType(final String filenameExtension) {
        if (filenameExtension.equalsIgnoreCase("bmp")) {
            return "image/bmp";
        }
        if (filenameExtension.equalsIgnoreCase("gif")) {
            return "image/gif";
        }
        if (filenameExtension.equalsIgnoreCase("jpeg") || filenameExtension.equalsIgnoreCase("jpg") || filenameExtension.equalsIgnoreCase("png")) {
            return "image/jpeg";
        }
        if (filenameExtension.equalsIgnoreCase("html")) {
            return "text/html";
        }
        if (filenameExtension.equalsIgnoreCase("txt")) {
            return "text/plain";
        }
        if (filenameExtension.equalsIgnoreCase("vsd")) {
            return "application/vnd.visio";
        }
        if (filenameExtension.equalsIgnoreCase("pptx") || filenameExtension.equalsIgnoreCase("ppt")) {
            return "application/vnd.ms-powerpoint";
        }
        if (filenameExtension.equalsIgnoreCase("docx") || filenameExtension.equalsIgnoreCase("doc")) {
            return "application/msword";
        }
        if (filenameExtension.equalsIgnoreCase("xml")) {
            return "text/xml";
        }
        if (filenameExtension.equalsIgnoreCase("pdf")) {
            return "application/pdf";
        }
        return "image/jpeg";
    }
    
    public static String getFileTempUrl(final String fileName) throws Exception {
        if (!StringUtils.isEmpty((Object)fileName)) {
            return getFilePath(OSSClientUtil.dir + fileName);
        }
        return null;
    }
    
    public static String getFilePath(final String path) throws Exception {
        if (!StringUtils.isEmpty((Object)path)) {
            return getFileUrl(path, null);
        }
        return null;
    }
    
    public static String getFilePath(final String path, final Long minute) throws Exception {
        if (!StringUtils.isEmpty((Object)path)) {
            return getFileUrl(path, minute);
        }
        return null;
    }
    
    public static String getFileUrl(final String key, final Long minute) throws Exception {
        Date expiration = new Date(new Date().getTime() + 1800000L);
        if (minute != null) {
            expiration = new Date(new Date().getTime() + minute * 60L * 1000L);
        }
        try {
            final URL url = OSSClientUtil.ossClient.generatePresignedUrl(OSSClientUtil.bucketName, key, expiration);
            if (url != null) {
                return url.toString();
            }
        }
        catch (Exception e) {
            throw new Exception("\u7372\u53d6\u8def\u5f91\u5931\u6557!");
        }
        return null;
    }
    
    public static void deleteTempFile(final String fileName) throws Exception {
        if (!StringUtils.isEmpty((Object)fileName)) {
            deleteObject(OSSClientUtil.dir + fileName);
        }
    }
    
    public static void deleteObject(final String key) throws Exception {
        try {
            OSSClientUtil.ossClient.deleteObject(OSSClientUtil.bucketName, key);
        }
        catch (Exception e) {
            throw new Exception("\u522a\u9664\u5c0d\u8c61\u5931\u6557!");
        }
    }
    
    public static String copyFile(final String fileName, final Integer id, final Integer type, final Integer subType) throws Exception {
        return null;
    }
    
    public static String copyObject(final String srcKey, final String destKey) throws Exception {
        try {
            String eTag = null;
            final CopyObjectResult result = OSSClientUtil.ossClient.copyObject(OSSClientUtil.bucketName, srcKey, OSSClientUtil.bucketName, destKey);
            eTag = result.getETag();
            return eTag;
        }
        catch (Exception e) {
            throw new Exception("\u8907\u88fd\u5c0d\u8c61\u5931\u6557!");
        }
    }
    
    public static Long getFileSize(final String path) throws Exception {
        Long size = null;
        final Long contentLength = getObjectMetadata(path).getContentLength();
        if (contentLength != null) {
            size = contentLength;
        }
        return size;
    }
    
    public static ObjectMetadata getObjectMetadata(final String path) throws Exception {
        try {
            final ObjectMetadata metadata = OSSClientUtil.ossClient.getObjectMetadata(OSSClientUtil.bucketName, path);
            return metadata;
        }
        catch (Exception e) {
            throw new Exception("\u8907\u88fd\u5c0d\u8c61\u5931\u6557!");
        }
    }
    
    static {
        OSSClientUtil.endPoint = "oss-cn-shenzhen.aliyuncs.com";
        OSSClientUtil.accessKeyId = "LTAIdnqDZQJEbln7";
        OSSClientUtil.accessKeySecret = "nYun4JUhaiwslAr40uaGhUB6CTm50s";
        OSSClientUtil.bucketName = "uhome-media";
        OSSClientUtil.dir = "uhome/temp/";
    }
}
