
package com.smile.cloud.ossConfig.oss;

public enum HttpMethod
{
    DELETE, 
    GET, 
    HEAD, 
    POST, 
    PUT, 
    OPTIONS;
}
