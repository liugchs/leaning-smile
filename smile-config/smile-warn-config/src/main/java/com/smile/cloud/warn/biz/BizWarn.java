package com.smile.cloud.warn.biz;


import com.smile.cloud.common.base.result.CommonResult;

import java.lang.annotation.*;

/**
 * 业务异常预警
 * 异常捕获注解-该注解使用应与controller避开。
 *
 * @author LGC
 * @date 2021/7/22 15:12
 */
@Target({ElementType.PARAMETER, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
public @interface BizWarn {

//    /**
//     * 是否发送 netty
//     */
//    boolean send_netty() default true;
//
//    /**
//     * netty 指令  send_netty 为true时使用
//     */
//    CommandBaseEnum netty_cmd() default CommandBaseEnum.ERROR;

    /**
     * 异常描述
     */
    String desc() default "";

    /**
     * 是否处理返回值
     */
    boolean result_handle() default false;

    /**
     * 返回值类型
     * result_handle 为true时使用
     */
    Class<?> result_class() default CommonResult.class;

    /**
     * 是否发送到钉钉群
     */
    boolean dingTalk_handle() default false;


}
