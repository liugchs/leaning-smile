package com.smile.cloud.warn.constant;

/**
 * @author xunyong
 * @date 2021/9/9 16:46
 * @copyright 2021 mofang. All rights reserved
 */
public class DingTalkConstant {

    public static final String secret = "SECb1d85a78c6f04c2e26c8c5591beefc6e583e65a55ac357fcee85c39c5421d350";

    public static final String url = "https://oapi.dingtalk.com/robot/send?access_token=94307fa915032f2243c18994040b12c347ab67dddb4a5f74b7b142683a079a16";
}
