package com.smile.cloud.zkConfig.distributedLock.zk;//package com.smile.cloud.distributedLock;
//
//import com.smile.cloud.base.model.User;
//import com.smile.cloud.base.result.data.DataResult;
//import com.smile.cloud.base.service.ConvertService;
//import com.smile.cloud.base.service.RedisService;
//import com.smile.cloud.distributedLock.redisson.RedissonLocker;
//import com.smile.cloud.distributedLock.zk.ZookeeperLocker;
//import io.swagger.annotations.Api;
//import io.swagger.annotations.ApiOperation;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.bind.annotation.*;
//
//import javax.annotation.PostConstruct;
//import java.time.LocalDateTime;
//import java.util.ArrayList;
//import java.util.List;
//import java.util.concurrent.ExecutorService;
//import java.util.concurrent.Executors;
//import java.util.function.BiConsumer;
//
///**
// * @author smile
// * @version V1.0
// * @className RedisController
// * @description TODO
// * @date 2021/3/6
// **/
//@Slf4j
//@Api(value = "分布式锁")
//@RestController
//@RequestMapping("/api/lock")
//public class LockController {
//    /**
//     * 库存key
//     */
//    private final static String stockKey = "stock";
//
//    @Autowired
//    private RedisService redisService;
//    @Autowired
//    private RedissonLocker redissonLocker;
//    @Autowired
//    private ZookeeperLocker zookeeperLocker;
//
//    /**
//     * 初始化200个库存
//     */
//    @PostConstruct
//    private void init() {
//        redisService.set(stockKey, 999999);
//    }
//
//
//    @GetMapping("/addStock/{num}")
//    @ApiOperation("redisson锁")
//    public DataResult<Integer> redissonLock(@PathVariable("num") Integer num) throws Exception {
//      /*  redisService.set(stockKey, 200);
//        ExecutorService executorService = Executors.newCachedThreadPool();
//        for (int i = 0; i < 200; i++) {
//            executorService.execute(new RedissonLockWorker(lockKey));
//        }
//        executorService.shutdown();*/
//        redisService.set(stockKey, num);
//
//
//        return DataResult.success(null);
//
//    }
//
//    @GetMapping("/redisson")
//    @ApiOperation("redisson锁")
//    public DataResult<Integer> redissonLock(@RequestParam("key") String lockKey) throws Exception {
//        redisService.set(stockKey, 200);
//        ExecutorService executorService = Executors.newCachedThreadPool();
//        for (int i = 0; i < 200; i++) {
//            executorService.execute(new RedissonLockWorker(lockKey));
//        }
//        executorService.shutdown();
//        return DataResult.success(1);
////        return DataResult.success(decreaseStock());
//
//    }
//
//    @GetMapping("/zk")
//    @ApiOperation("zookeeper锁")
//    public DataResult<Integer> zookeeperLock(@RequestParam("key") String lockKey) throws Exception {
//        ExecutorService executorService = Executors.newCachedThreadPool();
//        for (int i = 0; i < 200; i++) {
//            executorService.execute(new ZooKeeperLockWorker(lockKey));
//        }
//        executorService.shutdown();
//        return DataResult.success(1);
////        return DataResult.success(decreaseStock());
//
//    }
//
//
//    class RedissonLockWorker implements Runnable {
//        String lockKey;
//
//        public RedissonLockWorker(String lockKey) {
//            this.lockKey = lockKey;
//        }
//
//        @Override
//        public void run() {
//            try {
//                redissonLocker.lock(lockKey, new AquiredLockWorker<Integer>() {
//                    @Override
//                    public Integer invokeAfterLockAquire() {
//                        return decreaseStock();
//                    }
//                });
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        }
//
//    }
//
//    class ZooKeeperLockWorker implements Runnable {
//        String lockKey;
//
//        public ZooKeeperLockWorker(String lockKey) {
//            this.lockKey = lockKey;
//        }
//
//        @Override
//        public void run() {
//            try {
//                zookeeperLocker.lock(lockKey, new AquiredLockWorker<Integer>() {
//                    @Override
//                    public Integer invokeAfterLockAquire() {
//                        return decreaseStock();
//                    }
//                });
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        }
//
//    }
//
//
//    /**
//     * 减少库存
//     *
//     * @return 库存数量
//     */
//    private Integer decreaseStock() {
//        System.out.println(Thread.currentThread().getName() + " ---------- " + LocalDateTime.now());
//        System.out.println(Thread.currentThread().getName() + " start");
//        // 减库存
//        Integer stocks = (Integer) redisService.get(stockKey);
//        if (stocks > 0) {
//            stocks--;
//            redisService.set(stockKey, stocks);
//            System.out.println("剩余库存：" + stocks);
//        }
//        System.out.println(Thread.currentThread().getName() + " end");
//        System.out.println(Thread.currentThread().getName() + " ---------- " + LocalDateTime.now());
//        return stocks;
//    }
//}
