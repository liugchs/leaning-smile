package com.smile.cloud.zkConfig.distributedLock.zk;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;


///**
// * v3
// * @author MF2
// */
//@Configuration
//@PropertySource("classpath:zookeeper.properties")
//@ConfigurationProperties(prefix = "zookeeper", ignoreUnknownFields = false)
//public class ZookeeperConfig {
//
//    private String server;
//
//    private String lockPath;
//
//    @Bean(initMethod = "init", destroyMethod = "destroy")
//    public ZookeeperClient zookeeperClient() {
//        return new ZookeeperClient(server, lockPath);
//    }
//
//}


////v2
//@Configuration
//@PropertySource("classpath:zookeeper.properties")
//public class ZookeeperConfig {
//
//    @Value("zookeeper.server")
//    private String server;
//
//    @Value("zookeeper.lockPath")
//    private String lockPath;
//
//    @Bean(initMethod = "init", destroyMethod = "destroy")
//    public ZookeeperClient zookeeperClient() {
//        return new ZookeeperClient(server, lockPath);
//    }
//}


//v1
@Configuration
@PropertySource("classpath:zookeeper.properties")
public class ZookeeperConfig {

    @Autowired
    private Environment environment;

    @Bean(initMethod = "init", destroyMethod = "destroy")
    public ZookeeperClient zookeeperClient() {
        String zookeeperServer = environment.getRequiredProperty("zookeeper.server");
        String zookeeperLockPath = environment.getRequiredProperty("zookeeper.lockPath");
        return new ZookeeperClient(zookeeperServer, zookeeperLockPath);
    }

}

