package com.smile.cloud.zkConfig.distributedLock.zk;

import java.util.concurrent.TimeUnit;

/**
 * 分布式锁
 */
public interface ZKLocker {

    long TIME_OUT = 5;

    <T> T lock(String resourceName, ZKAquiredLockWorker<T> worker) throws UnableToZKAquireLockException, Exception;

    <T> T lock(String resourceName, ZKAquiredLockWorker<T> worker, long lockTime, TimeUnit timeUnit) throws UnableToZKAquireLockException, Exception;
}