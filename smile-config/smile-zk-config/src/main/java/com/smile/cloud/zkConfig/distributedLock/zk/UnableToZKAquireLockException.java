package com.smile.cloud.zkConfig.distributedLock.zk;

public class UnableToZKAquireLockException extends RuntimeException {

    public UnableToZKAquireLockException() {
    }

    public UnableToZKAquireLockException(String message) {
        super(message);
    }

    public UnableToZKAquireLockException(String message, Throwable cause) {
        super(message, cause);
    }
}