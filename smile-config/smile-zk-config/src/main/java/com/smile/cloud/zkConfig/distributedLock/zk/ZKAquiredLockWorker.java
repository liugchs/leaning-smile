package com.smile.cloud.zkConfig.distributedLock.zk;

/**
 * 获取锁后执行的方法
 *
 * @param <T>
 */
public interface ZKAquiredLockWorker<T> {
    T invokeAfterLockAquire() throws Exception;
}