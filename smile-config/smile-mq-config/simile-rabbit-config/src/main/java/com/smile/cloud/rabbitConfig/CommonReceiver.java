package com.smile.cloud.rabbitConfig;

import com.rabbitmq.client.Channel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import java.io.IOException;

/**
 * 订单消息的处理者
 *
 * @author LGC
 */
@Component
@Slf4j
public class CommonReceiver {

//    @Bean
//    public Queue commonQueue() {
//        return new Queue(QueueBingEnum.QUEUE_COMMON.getQueueName(), true);
//    }


//    @RabbitListener(queues = "#{commonQueue.name}")
//    @RabbitHandler
//    public void generateHandle(Object objPram) {
//        log.info("receive message :{}", objPram);
//    }


    @RabbitListener(queues = "#{commonQueue.name}")
    @RabbitHandler
    public void generateHandle(Message message, Channel channel) throws IOException {
//        log.info("receive message :{}", message);
//        // 每次只接收一个信息
//        channel.basicQos(1);
//        String msg = new String(message.getBody(), "UTF-8");
//        String messageId = message.getMessageProperties().getMessageId();
//        JSONObject jsonObject = JSONObject.parseObject(msg);
//        try {
//            //channel.waitForConfirms()回调生产者confirm方法，只要回调成功了confirm，返回就是true，即使confrim里执行本地事务操作发生错误也是true
//            if (channel.waitForConfirms()) {
//                //redis消息ID不为空说明该消息在事务操作发生了错误
//                if (RedisPool.getJedis().get(messageId) != null) {
//                    // 丢弃该消息
//                    channel.basicNack(message.getMessageProperties().getDeliveryTag(), false, false);
//                    //redis删除消息ID
//                    RedisPool.getJedis().del(messageId);
//                } else {
//                    //执行更新数据库，可靠性投递机制，执行前要先查询一下数据库是否存在，避免签收前mq挂掉，重启后重发导致重复消费。
//                    //db略
//                    //消息确认
//                    channel.basicAck(message.getMessageProperties().getDeliveryTag(), true);
//                    System.out.println("消费消息：" + jsonObject);
//                }
//            }
//        } catch (Exception e) {
//            // 丢弃该消息
//            channel.basicNack(message.getMessageProperties().getDeliveryTag(), false, false);
//            //失败后消息被确认
//            // channel.basicReject(message.getMessageProperties().getDeliveryTag(), false);
//            //失败后消息重新放回队列，一般不用这个，可能会导致无限循环
//            // channel.basicReject(message.getMessageProperties().getDeliveryTag(), true);
//            System.out.println("签收失败:" + jsonObject);
//        }
    }
}
