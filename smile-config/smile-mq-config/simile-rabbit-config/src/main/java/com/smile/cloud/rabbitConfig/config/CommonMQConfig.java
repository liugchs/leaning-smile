package com.smile.cloud.rabbitConfig.config;

import org.springframework.amqp.core.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 公共消息队列配置
 * 初始化 交换机-队列-路由键之间的绑定
 *
 * @author LGC
 */
@Configuration
public class CommonMQConfig {

    /**
     * 创建公共交换机
     */
    @Bean
    DirectExchange commonDirect() {
        return ExchangeBuilder
                .directExchange(CommonQueueBingEnum.QUEUE_COMMON.getExchange())
                .durable(true)
                .build();
    }

    /**
     * 创建公共消息队列
     */
    @Bean
    public Queue commonQueue() {
        return new Queue(CommonQueueBingEnum.QUEUE_COMMON.getQueueName(), true);
    }


    /**
     * 公共消息队列绑定到公共交换机-通过路由键
     */
    @Bean
    Binding commonBinding(DirectExchange commonDirect, Queue commonQueue) {
        return BindingBuilder
                .bind(commonQueue)
                .to(commonDirect)
                .with(CommonQueueBingEnum.QUEUE_COMMON.getRouteKey());
    }


}
