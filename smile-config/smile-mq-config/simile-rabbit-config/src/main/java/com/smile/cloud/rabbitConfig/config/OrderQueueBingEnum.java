package com.smile.cloud.rabbitConfig.config;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * 消息队列交换机 队列 路由键绑定枚举
 *
 * @author LGC
 */
@Getter
@AllArgsConstructor
@NoArgsConstructor
public enum OrderQueueBingEnum {

    /**
     * 生成订单消息通知队列
     */
    QUEUE_ORDER_GENERATE("生成订单消息通知", "smile.order.direct", "smile.order.generate.queue", "smile.order.generate.roueKey"),
    /**
     * 取消订单消息通知队列
     */
    QUEUE_ORDER_CANCEL("取消订单消息通知", "smile.order.direct", "smile.order.cancel.queue", "smile.order.cancel.roueKey"),
    /**
     * 取消订单消息通知ttl队列
     */
    QUEUE_ORDER_CANCEL_TTL("取消订单消息通知ttl", "smile.order.direct.ttl", "smile.order.cancel.ttl.queue", "smile.order.cancel.ttl.roueKey");

    /**
     * 描述
     */
    private String desc;

    /**
     * 交换名称
     */
    private String exchange;
    /**
     * 队列名称
     */
    private String queueName;
    /**
     * 路由键
     */
    private String routeKey;

}
