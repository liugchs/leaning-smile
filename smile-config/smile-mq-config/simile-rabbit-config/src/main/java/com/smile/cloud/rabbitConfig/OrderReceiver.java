package com.smile.cloud.rabbitConfig;

import com.smile.cloud.rabbitConfig.service.OrderService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 订单消息的处理者
 */
@Component
@Slf4j
public class OrderReceiver {
    @Autowired
    private OrderService orderService;

//    @Bean
//    public Queue orderCancelQueue() {
//        return new Queue(QueueBingEnum.QUEUE_ORDER_CANCEL.getQueueName(),true);
//    }
//
//
//    @Bean
//    public Queue orderGenerateQueue() {
//        return new Queue(QueueBingEnum.QUEUE_ORDER_GENERATE.getQueueName(),true);
//    }


    @RabbitListener(queues = "#{cancelOrderQueue.name}")
    @RabbitHandler
    public void cancelHandle(Long orderId) {
        log.info("receive cancel order message orderId:{}", orderId);
        //执行取消订单流程
        orderService.cancelOrder(orderId);
    }


    @RabbitListener(queues = "#{generateOrderQueue.name}")
    @RabbitHandler
    public void generateHandle(Object objPram) {
        log.info("receive generate order message :{}", objPram);
    }
}
