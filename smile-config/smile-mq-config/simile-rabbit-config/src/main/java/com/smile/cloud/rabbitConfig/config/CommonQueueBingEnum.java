package com.smile.cloud.rabbitConfig.config;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * 消息队列交换机 队列 路由键绑定枚举
 *
 * @author LGC
 */
@Getter
@AllArgsConstructor
@NoArgsConstructor
public enum CommonQueueBingEnum {

    /**
     * 公共普通消息通知
     */
    QUEUE_COMMON("公共-消息通知", "smile.common.direct", "smile.common.queue", "smile.common.roueKey"),
    ;

    /**
     * 描述
     */
    private String desc;

    /**
     * 交换名称
     */
    private String exchange;
    /**
     * 队列名称
     */
    private String queueName;
    /**
     * 路由键
     */
    private String routeKey;

}
