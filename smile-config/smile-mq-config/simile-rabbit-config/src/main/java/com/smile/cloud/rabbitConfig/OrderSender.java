package com.smile.cloud.rabbitConfig;

import com.smile.cloud.rabbitConfig.config.OrderQueueBingEnum;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.AmqpException;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessagePostProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 订单消息的发出者
 */
@Slf4j
@Component
public class OrderSender {

    @Autowired
    private AmqpTemplate amqpTemplate;

    /**
     * 发送订单取消延时消息
     *
     * @param orderId
     * @param delayTimes
     */
    public void sendOrderCancelDelayMessage(Long orderId, final long delayTimes) {
        //给延迟队列发送消息
        amqpTemplate.convertAndSend(OrderQueueBingEnum.QUEUE_ORDER_CANCEL_TTL.getExchange(), OrderQueueBingEnum.QUEUE_ORDER_CANCEL_TTL.getRouteKey(), orderId, new MessagePostProcessor() {
            @Override
            public Message postProcessMessage(Message message) throws AmqpException {
                //给消息设置延迟毫秒值
                message.getMessageProperties().setExpiration(String.valueOf(delayTimes));
                return message;
            }
        });
        log.info("send cancel order delay message orderId:{}", orderId);
    }

    /**
     * 发送订单生成消息
     *
     * @param objParam
     */
    public void sendOrderGenerateMessage(Object objParam) {
        //给延迟队列发送消息
        amqpTemplate.convertAndSend(OrderQueueBingEnum.QUEUE_ORDER_GENERATE.getExchange(), OrderQueueBingEnum.QUEUE_ORDER_GENERATE.getRouteKey(), objParam);
        log.info("send order generate message orderParam:{}", objParam);
    }

    /**
     * 发送订单取消消息
     *
     * @param objParam
     */
    public void sendOrderCancelMessage(Object objParam) {
        //给延迟队列发送消息
        amqpTemplate.convertAndSend(OrderQueueBingEnum.QUEUE_ORDER_CANCEL.getExchange(), OrderQueueBingEnum.QUEUE_ORDER_CANCEL.getRouteKey(), objParam);
        log.info("send order generate message orderParam:{}", objParam);
    }
}
