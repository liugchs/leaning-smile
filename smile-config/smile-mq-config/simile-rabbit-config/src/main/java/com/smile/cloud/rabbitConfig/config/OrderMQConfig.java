package com.smile.cloud.rabbitConfig.config;

import org.springframework.amqp.core.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 订单消息队列配置
 * 初始化 交换机-队列-路由键之间的绑定
 *
 * @author LGC
 */
@Configuration
public class OrderMQConfig {

    /**
     * 创建订单消息实际消费队列所绑定的交换机
     */
    @Bean
    DirectExchange generateOrderDirect() {
        return ExchangeBuilder
                .directExchange(OrderQueueBingEnum.QUEUE_ORDER_GENERATE.getExchange())
                .durable(true)
                .build();
    }

    /**
     * 取消订单消息实际消费队列所绑定的交换机
     */
    @Bean
    DirectExchange cancelOrderDirect() {
        return ExchangeBuilder
                .directExchange(OrderQueueBingEnum.QUEUE_ORDER_CANCEL.getExchange())
                .durable(true)
                .build();
    }

    /**
     * 取消订单延迟队列队列所绑定的交换机
     */
    @Bean
    DirectExchange cancelOrderTtlDirect() {
        return ExchangeBuilder
                .directExchange(OrderQueueBingEnum.QUEUE_ORDER_CANCEL_TTL.getExchange())
                .durable(true)
                .build();
    }

    /**
     * 创建订单实际消费队列
     */
    @Bean
    public Queue generateOrderQueue() {
        return new Queue(OrderQueueBingEnum.QUEUE_ORDER_GENERATE.getQueueName(), true);
    }

    /**
     * 取消订单实际消费队列
     */
    @Bean
    public Queue cancelOrderQueue() {
        return new Queue(OrderQueueBingEnum.QUEUE_ORDER_CANCEL.getQueueName(), true);
    }

    /**
     * 取消订单延迟队列（死信队列）
     */
    @Bean
    public Queue cancelOrderTtlQueue() {
        return QueueBuilder
                .durable(OrderQueueBingEnum.QUEUE_ORDER_CANCEL_TTL.getQueueName())
                //到期后转发的交换机
                .withArgument("x-dead-letter-exchange", OrderQueueBingEnum.QUEUE_ORDER_CANCEL.getExchange())
                //到期后转发的路由键
                .withArgument("x-dead-letter-routing-key", OrderQueueBingEnum.QUEUE_ORDER_CANCEL.getRouteKey())
                .build();

//        Map<String, Object> arguments = new HashMap<>(2);
//        // 绑定我们的死信交换机
//        arguments.put("x-dead-letter-exchange", OrderQueueEnum.QUEUE_ORDER_CANCEL.getExchange());
//        // 绑定我们的路由key
//        arguments.put("x-dead-letter-routing-key", OrderQueueEnum.QUEUE_ORDER_CANCEL.getRouteKey());
//        return new Queue(OrderQueueEnum.QUEUE_TTL_ORDER_CANCEL.getQueueName(), true, false, false, arguments);

    }

    /**
     * 将创建订单队列绑定到取消交换机
     */
    @Bean
    Binding generateOrderBinding(DirectExchange generateOrderDirect, Queue generateOrderQueue) {
        return BindingBuilder
                .bind(generateOrderQueue)
                .to(generateOrderDirect)
                .with(OrderQueueBingEnum.QUEUE_ORDER_GENERATE.getRouteKey());
    }

    /**
     * 将取消订单队列绑定到取消交换机
     */
    @Bean
    Binding cancelOrderBinding(DirectExchange cancelOrderDirect, Queue cancelOrderQueue) {
        return BindingBuilder
                .bind(cancelOrderQueue)
                .to(cancelOrderDirect)
                .with(OrderQueueBingEnum.QUEUE_ORDER_CANCEL.getRouteKey());
    }

    /**
     * 将取消订单延迟队列绑定到交换机
     */
    @Bean
    Binding cancelOrderTtlBinding(DirectExchange cancelOrderTtlDirect, Queue cancelOrderTtlQueue) {
        return BindingBuilder
                .bind(cancelOrderTtlQueue)
                .to(cancelOrderTtlDirect)
                .with(OrderQueueBingEnum.QUEUE_ORDER_CANCEL_TTL.getRouteKey());
    }

}
