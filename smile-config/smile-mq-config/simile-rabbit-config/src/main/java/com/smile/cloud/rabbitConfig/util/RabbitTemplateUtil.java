package com.smile.cloud.rabbitConfig.util;

import cn.hutool.json.JSONObject;
import com.rabbitmq.tools.json.JSONUtil;
import com.smile.cloud.rabbitConfig.config.CommonQueueBingEnum;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.connection.CorrelationData;
import org.springframework.amqp.rabbit.core.RabbitTemplate;

import java.util.UUID;

/**
 * @author LGC
 * @version V1.0
 * @date 2021/7/19
 **/
@Slf4j
public class RabbitTemplateUtil {

    private RabbitTemplate rabbitTemplate;

    public RabbitTemplateUtil(RabbitTemplate rabbitTemplate) {
        this.rabbitTemplate = rabbitTemplate;
    }


    /**
     * 给公共消息队列发送消息
     *
     * @param sendObj
     */
    public <T> void sendMessage(T sendObj) {
        //给延迟队列发送消息
        rabbitTemplate.convertAndSend(CommonQueueBingEnum.QUEUE_COMMON.getExchange(), CommonQueueBingEnum.QUEUE_COMMON.getRouteKey(), sendObj);
        log.info("send message :{}", sendObj);
    }

    /**
     * 获取发送MQ消息实体
     */
    public <T> void sendMessage(CommonQueueBingEnum commonQueueBingEnum, T real_data) {
//        log.info(
//                "发送消息到RabbitMQ...., request_dto: " + JSONObject.toJSONString(real_data) + ", config：" + JSONObject.toJSONString(commonQueueBingEnum));
//        CorrelationData correlationData = new CorrelationData(UUID.randomUUID().toString());
//        rabbitTemplate.convertAndSend(commonQueueBingEnum.getExchange(), commonQueueBingEnum.getRouteKey(), JSONUtil.toJsonStr(real_data), correlationData);
    }


}
