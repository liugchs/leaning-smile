package com.smile.cloud.rabbitConfig.config;

import com.smile.cloud.rabbitConfig.util.RabbitTemplateUtil;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.annotation.Resource;


/**
 * @author LGC
 */
@Configuration
public class RabbitTemplateConfig {

    @Resource
    private RabbitTemplate rabbitTemplate;

    @Resource
    private RabbitCallBackConfig rabbitCallBackConfig;

    @Bean("rabbitTemplateUtil")
    public RabbitTemplateUtil rabbitTemplateUtil() {
        RabbitTemplateUtil rabbitUtil = new RabbitTemplateUtil(rabbitTemplate);
        rabbitTemplate.setConfirmCallback(rabbitCallBackConfig);
        rabbitTemplate.setReturnCallback(rabbitCallBackConfig);
        rabbitTemplate.setReplyTimeout(6000);
        return rabbitUtil;
    }

}
