package com.smile.cloud.redisConfig.distributedLock.redisson.redissonProperties;

import lombok.Data;
import lombok.ToString;


@Data
@ToString
public class RedisPoolProperties {

    private int maxIdle;

    private int minIdle;

    private int maxActive;

    private int maxWait;

    private int connTimeout;

    private int soTimeout;

    /**
     * 池大小
     */
    private int size;

}
