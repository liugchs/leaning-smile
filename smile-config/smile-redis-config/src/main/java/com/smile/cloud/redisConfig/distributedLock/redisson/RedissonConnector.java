package com.smile.cloud.redisConfig.distributedLock.redisson;//package com.smile.cloud.admin.distributedLock.redisson;
//
//import lombok.Data;
//import lombok.ToString;
//import org.redisson.Redisson;
//import org.redisson.api.RedissonClient;
//import org.redisson.config.Config;
//import org.springframework.boot.context.properties.ConfigurationProperties;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//
//import javax.annotation.PostConstruct;
//import java.io.IOException;
//
///**
// * redisson配置类
// */
//@Configuration
//public class RedissonConnector {
//
//    /**
//     * 所有对redisson的使用都是通过RedissonClient来使用
//     *
//     * @return
//     * @throws IOException
//     */
//    //@Bean(destroyMethod = "shutdown")
//    @Bean(name = "redissonClient")
//    public RedissonClient redissonClient() throws IOException {
//        //1 创建配置
//        Config config = new Config();
//        config.useSingleServer().setAddress("redis://127.0.0.1:6379");
//       /* //集群配置
//        Config config = new Config();
//        config.useClusterServers()
//                .setScanInterval(2000) // 集群状态扫描间隔时间，单位是毫秒
//                //可以用"rediss://"来启用SSL连接
//                .addNodeAddress("redis://127.0.0.1:7000", "redis://127.0.0.1:7001")
//                .addNodeAddress("redis://127.0.0.1:7002");*/
//
//        return Redisson.create(config);
//    }
//
////    @PostConstruct
////    public void init() {
////        //1 创建配置
////        Config config = new Config();
////        config.useSingleServer().setAddress("redis://127.0.0.1:6379");
////       /* 2//集群配置
////        Config config = new Config();
////        config.useClusterServers()
////                .setScanInterval(2000) // 集群状态扫描间隔时间，单位是毫秒
////                //可以用"rediss://"来启用SSL连接
////                .addNodeAddress("redis://127.0.0.1:7000", "redis://127.0.0.1:7001")
////                .addNodeAddress("redis://127.0.0.1:7002");*/
////        client = Redisson.create();
////    }
//
//}
//
//
