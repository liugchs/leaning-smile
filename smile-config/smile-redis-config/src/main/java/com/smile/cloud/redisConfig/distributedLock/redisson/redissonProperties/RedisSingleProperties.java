package com.smile.cloud.redisConfig.distributedLock.redisson.redissonProperties;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class RedisSingleProperties {
    private String address;
}

