package com.smile.cloud.redisConfig.distributedLock.redisson;

import java.util.concurrent.TimeUnit;

/**
 * 分布式锁
 */
public interface DistributedLocker {

    long TIME_OUT = 5;

    <T> T lock(String resourceName, AquiredLockWorker<T> worker) throws UnableToAquireLockException, Exception;

    <T> T lock(String resourceName, AquiredLockWorker<T> worker, long lockTime, TimeUnit timeUnit) throws UnableToAquireLockException, Exception;
}