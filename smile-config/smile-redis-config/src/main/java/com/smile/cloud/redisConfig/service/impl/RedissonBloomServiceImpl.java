package com.smile.cloud.redisConfig.service.impl;

import com.smile.cloud.redisConfig.service.RedissonBloomFilterService;
import lombok.extern.slf4j.Slf4j;
import org.redisson.api.ExpiredObjectListener;
import org.redisson.api.RBloomFilter;
import org.redisson.api.RedissonClient;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;

/**
 * @author LGC
 */
@Slf4j
@Service
public class RedissonBloomServiceImpl implements RedissonBloomFilterService {

    @Resource
    private RedissonClient redissonClient;

    public static final String PREFIX = "redission_bloom_";

    private static final DateTimeFormatter YYYY_MM_DD = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static final int TIME_FORMAT_LENGTH = 8;

    /**
     * key为 prefix+20210718+key
     */
    private Map<String, ConcurrentHashMap<String, RBloomFilter<String>>> bloomFilterMap = new ConcurrentHashMap<>();

    @Override
    public synchronized boolean initNewFilter(String key, Long expireDate, TimeUnit timeUnit, long expectedInsertions, double falseProbability) {
        String timeFormatter = YYYY_MM_DD.format(LocalDateTime.now());
        RBloomFilter<String> bloomFilter = redissonClient.getBloomFilter(PREFIX + timeFormatter + key);
        /**
         * 初始化布隆过滤器
         * 存在初始化成功
         *  设置过期时间
         *  设置过期监听器
         *
         */
        boolean isSuccess = bloomFilter.tryInit(expectedInsertions, falseProbability);
        if (isSuccess) {
            //设置过期时间
            bloomFilter.expire(expireDate, timeUnit);

            //设置过期监听器
            bloomFilter.addListener(new ExpiredObjectListener() {
                @Override
                public void onExpired(String key) {
                    log.info("onExpired callback running key is {}", key);
                    if (key.startsWith(PREFIX)) {
                        String firstKey = key.substring(PREFIX.length() + TIME_FORMAT_LENGTH);
                        ConcurrentHashMap<String, RBloomFilter<String>> map = bloomFilterMap.get(firstKey);
                        if (Objects.isNull(map)) {
                            log.warn("listener callback key is empty key is {}", key);
                            return;
                        }
                        map.remove(key);
                    }
                }
            });

        }

        ConcurrentHashMap<String, RBloomFilter<String>> map = bloomFilterMap.computeIfAbsent(key, (k) -> {
            return new ConcurrentHashMap<String, RBloomFilter<String>>(4);
        });
        map.put(PREFIX + timeFormatter + key, bloomFilter);
        isSuccess = true;

        return isSuccess;
    }

    @Override
    public boolean add(String key, String value) {
        String timeFormatter = YYYY_MM_DD.format(LocalDateTime.now());
        ConcurrentHashMap<String, RBloomFilter<String>> map = bloomFilterMap.get(key);
        if (Objects.isNull(map)) {
            log.error("add name key one value is null, name is {},value is {}", key, value);
            return false;
        }
        RBloomFilter<String> rBloomFilter = map.get(PREFIX + timeFormatter + key);
        if (Objects.isNull(rBloomFilter)) {
            log.error("add name key one value is null, name is {},redis key is {},value is {}", key, PREFIX + timeFormatter + key, value);
            return false;
        }
        return rBloomFilter.add(value);
    }

    @Override
    public boolean add(String key, List<String> list) {
        String timeFormatter = YYYY_MM_DD.format(LocalDateTime.now());
        ConcurrentHashMap<String, RBloomFilter<String>> map = bloomFilterMap.get(key);
        if (Objects.isNull(map)) {
            log.error("add name key list value is null, name is {}", key);
            return false;
        }
        RBloomFilter<String> rBloomFilter = map.get(PREFIX + timeFormatter + key);
        if (Objects.isNull(rBloomFilter)) {
            log.error("add name key list value is null, name is {},redis key is {}", key, PREFIX + timeFormatter + key);
            return false;
        }
        list.forEach(value -> rBloomFilter.add(value));
        return true;
    }

    @Override
    public boolean containsValue(String key, String value) {
        ConcurrentHashMap<String, RBloomFilter<String>> map = bloomFilterMap.get(key);
        if (Objects.isNull(map)) {
            log.error("containsValue key is exist, key is {}", key);
            return false;
        }
        for (Map.Entry<String, RBloomFilter<String>> entry : map.entrySet()) {
            if (entry.getValue().contains(value)) {
                return true;
            }
        }
        return false;
    }


    @Override
    public boolean containKey(String key) {
        return bloomFilterMap.containsKey(key);
    }

}

