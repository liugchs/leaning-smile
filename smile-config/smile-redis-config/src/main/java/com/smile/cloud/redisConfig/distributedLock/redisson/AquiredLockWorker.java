package com.smile.cloud.redisConfig.distributedLock.redisson;

/**
 * 获取锁后执行的方法
 *
 * @param <T>
 */
public interface AquiredLockWorker<T> {
    T invokeAfterLockAquire() throws Exception;
}