package com.smile.cloud.nettyService;

import com.smile.cloud.nettyService.typeServer.TCPServer;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.env.Environment;

import java.net.InetAddress;

/**
 * @author LGC
 */
@Slf4j
@SpringBootApplication
public class SmileNettyServiceApplication {

    public static void main(String[] args) throws Exception {
        SpringApplication app = new SpringApplication(SmileNettyServiceApplication.class);
        ConfigurableApplicationContext run = app.run(args);
        Environment env = run.getEnvironment();
        log.info("\n---------------------------------------------------------\n\t" +
                        "Netty服务启动成功\n\t" +
                        "应用 '{}' 正在运行! 链接地址为:\n\t" +
                        "本地地址: \thttp://localhost:{}\n\t" +
                        "网络地址: \thttp://{}:{}\n\t" +
                        "接口文档: \thttp://{}:{}/doc.html\n" +
                        "----------------------------------------------------------",
                env.getProperty("spring.application.name"),
                env.getProperty("server.port"),
                InetAddress.getLocalHost().getHostAddress(),
                env.getProperty("server.port"),
                InetAddress.getLocalHost().getHostAddress(),
                env.getProperty("server.port"));

        // 启动Netty Server
        TCPServer tcpServer = run.getBean(TCPServer.class);
        tcpServer.start();
        log.info("Netty Server Poster:{}", tcpServer.getTcpPort().getHostString());
    }

}
