package com.smile.cloud.order.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.smile.cloud.order.model.Order;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 * @author LGC
 * @date 2021/8/17 11:12
 * @copyright 2021 mofang. All rights reserved
 */

public interface OrderMapper extends BaseMapper<Order> {
    int updateBatch(List<Order> list);

    int batchInsert(@Param("list") List<Order> list);

    int updateBatchSelective(List<Order> list);
}