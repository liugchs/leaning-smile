package com.smile.cloud.order.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.smile.cloud.order.model.TransactionLog;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author LGC
 * @date 2021/8/12 17:35
 * @copyright 2021 mofang. All rights reserved
 */

public interface TransactionLogMapper extends BaseMapper<TransactionLog> {
    int updateBatch(List<TransactionLog> list);

    int updateBatchSelective(List<TransactionLog> list);

    int batchInsert(@Param("list") List<TransactionLog> list);
}