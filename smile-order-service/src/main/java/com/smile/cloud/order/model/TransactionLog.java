package com.smile.cloud.order.model;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 事务日志表
 *
 * @author LGC
 * @date 2021/8/12 17:35
 * @copyright 2021 mofang. All rights reserved
 */

@ApiModel(value = "com-smile-cloud-order-model-TransactionLog")
@Data
@TableName(value = "transaction_log")
public class TransactionLog implements Serializable {
    private static final long serialVersionUID = -1883518727958622387L;
    /**
     * 事务ID
     */
    @TableId
    @ApiModelProperty(value = "事务ID")
    private String id;

    /**
     * 业务标识
     */
    @TableField(value = "business")
    @ApiModelProperty(value = "业务标识")
    private String business;

    /**
     * 对应业务表中的主键
     */
    @TableField(value = "foreign_key")
    @ApiModelProperty(value = "对应业务表中的主键")
    private String foreignKey;

}