package com.smile.cloud.order.service;

import org.apache.rocketmq.client.exception.MQClientException;

public interface OrderService {


    /**
     * 执行本地事务时调用，将订单数据和事务日志写入本地数据库
     *
     * @param orderDTO
     * @param transactionId
     */
    int createOrder(OrderDTO orderDTO, String transactionId) throws Exception;


    /**
     * 前端调用 创建订单 只用于向RocketMQ发送事务消息
     *
     * @param order
     * @throws MQClientException
     */
    void createOrderSendMsg(OrderDTO order) throws MQClientException;
}
