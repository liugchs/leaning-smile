package com.smile.cloud.order.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.math.BigDecimal;
import lombok.Data;

/**
 *
 * @author LGC
 * @date 2021/8/17 11:12
 * @copyright 2021 mofang. All rights reserved
 */

/**
 * 订单信息
 */
@ApiModel(value = "com-smile-cloud-order-model-Order")
@Data
@TableName(value = "`order`")
public class Order implements Serializable {
    /**
     * 订单ID
     */
    @TableId(value = "id")
    @ApiModelProperty(value = "订单ID")
    private String id;

    /**
     * 订单状态 0 未确认 1 已确认 2 已关闭
     */
    @TableField(value = "order_status")
    @ApiModelProperty(value = "订单状态 0 未确认 1 已确认 2 已关闭")
    private Integer orderStatus;

    /**
     * 支付状态:0 未支付 1 已支付 2 支付异常 3 已退款
     */
    @TableField(value = "pay_status")
    @ApiModelProperty(value = "支付状态:0 未支付 1 已支付 2 支付异常 3 已退款")
    private Integer payStatus;

    /**
     * 订单编号
     */
    @TableField(value = "order_no")
    @ApiModelProperty(value = "订单编号")
    private String orderNo;

    /**
     * 用户id
     */
    @TableField(value = "user_id")
    @ApiModelProperty(value = "用户id")
    private Integer userId;

    /**
     * 产品id
     */
    @TableField(value = "product_id")
    @ApiModelProperty(value = "产品id")
    private Integer productId;

    /**
     * 数量
     */
    @TableField(value = "`count`")
    @ApiModelProperty(value = "数量")
    private Integer count;

    /**
     * 金额
     */
    @TableField(value = "money")
    @ApiModelProperty(value = "金额")
    private BigDecimal money;

    private static final long serialVersionUID = 1L;
}