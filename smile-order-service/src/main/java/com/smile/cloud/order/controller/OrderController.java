package com.smile.cloud.order.controller;

import com.smile.cloud.order.service.OrderDTO;
import com.smile.cloud.order.service.OrderService;
import io.swagger.annotations.ApiOperation;
import org.apache.rocketmq.client.exception.MQClientException;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author LGC
 * @date 2021/8/12 18:31
 * @copyright 2021 mofang. All rights reserved
 */
@RestController
public class OrderController {
    @Resource
    private OrderService orderService;

    @ApiOperation(value = "事务订单，发送延时消息通知，订单是否支付，未支付订单关闭，释放库存")
    @PostMapping("order/create")
    public void create(@RequestBody OrderDTO dto) throws MQClientException {
        orderService.createOrderSendMsg(dto);
    }
}
