package com.smile.cloud.mbg.base.config;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;

/**
 * @ClassName MybatisPlusMetaObjectHandler
 * @Description 自动补充插入或更新时的值
 * @author smile
 * @Date 2020/3/19
 * @Version V1.0
 **/
@Component
public class MybatisPlusMetaObjectHandler implements MetaObjectHandler {
    @Autowired
    private HttpServletRequest servletRequest;

    private Integer userId;

    @Override
    public void insertFill(MetaObject metaObject) {
//        this.userId = (int) jwtUtils.getUserFromRequest(servletRequest).getId();
        this.setFieldValByName("createdBy", userId, metaObject);
        this.setFieldValByName("createdTime", LocalDateTime.now(), metaObject);
        this.setFieldValByName("updatedBy", userId, metaObject);
        this.setFieldValByName("updatedTime", LocalDateTime.now(), metaObject);

    }

    @Override
    public void updateFill(MetaObject metaObject) {
//        this.userId = (int) jwtUtils.getUserFromRequest(servletRequest).getId();
        this.setFieldValByName("updatedBy", userId, metaObject);
        this.setFieldValByName("updatedTime", LocalDateTime.now(), metaObject);
    }

}