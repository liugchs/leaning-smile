package com.smile.cloud.mbg.base.service.impl;

import com.smile.cloud.mbg.base.model.SysSystem;
import com.smile.cloud.mbg.base.mapper.SysSystemMapper;
import com.smile.cloud.mbg.base.service.SysSystemService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author smile
 * @since 2020-12-31
 */
@Service
public class SysSystemServiceImpl extends ServiceImpl<SysSystemMapper, SysSystem> implements SysSystemService {

}
