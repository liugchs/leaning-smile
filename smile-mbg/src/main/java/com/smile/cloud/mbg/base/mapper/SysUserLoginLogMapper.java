package com.smile.cloud.mbg.base.mapper;

import com.smile.cloud.mbg.base.model.SysUserLoginLog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 用户登录日志表 Mapper 接口
 * </p>
 *
 * @author smile
 * @since 2020-12-31
 */
public interface SysUserLoginLogMapper extends BaseMapper<SysUserLoginLog> {

}
