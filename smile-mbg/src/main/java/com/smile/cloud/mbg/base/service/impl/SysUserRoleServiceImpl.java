package com.smile.cloud.mbg.base.service.impl;

import com.smile.cloud.mbg.base.model.SysUserRole;
import com.smile.cloud.mbg.base.mapper.SysUserRoleMapper;
import com.smile.cloud.mbg.base.service.SysUserRoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 账号和角色关系表 服务实现类
 * </p>
 *
 * @author smile
 * @since 2020-12-31
 */
@Service
public class SysUserRoleServiceImpl extends ServiceImpl<SysUserRoleMapper, SysUserRole> implements SysUserRoleService {

}
