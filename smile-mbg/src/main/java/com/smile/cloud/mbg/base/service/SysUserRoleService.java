package com.smile.cloud.mbg.base.service;

import com.smile.cloud.mbg.base.model.SysUserRole;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 账号和角色关系表 服务类
 * </p>
 *
 * @author smile
 * @since 2020-12-31
 */
public interface SysUserRoleService extends IService<SysUserRole> {

}
