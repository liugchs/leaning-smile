package com.smile.cloud.mbg.base.service;

import com.smile.cloud.mbg.base.model.SysUser;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 用户账号表 服务类
 * </p>
 *
 * @author smile
 * @since 2020-12-31
 */
public interface SysUserService extends IService<SysUser> {

}
