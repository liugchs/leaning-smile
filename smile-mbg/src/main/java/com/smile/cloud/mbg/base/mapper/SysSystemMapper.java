package com.smile.cloud.mbg.base.mapper;

import com.smile.cloud.mbg.base.model.SysSystem;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author smile
 * @since 2020-12-31
 */
public interface SysSystemMapper extends BaseMapper<SysSystem> {

}
