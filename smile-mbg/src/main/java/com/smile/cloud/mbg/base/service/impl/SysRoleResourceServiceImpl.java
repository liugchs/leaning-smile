package com.smile.cloud.mbg.base.service.impl;

import com.smile.cloud.mbg.base.model.SysRoleResource;
import com.smile.cloud.mbg.base.mapper.SysRoleResourceMapper;
import com.smile.cloud.mbg.base.service.SysRoleResourceService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 角色资源关系表 服务实现类
 * </p>
 *
 * @author smile
 * @since 2020-12-31
 */
@Service
public class SysRoleResourceServiceImpl extends ServiceImpl<SysRoleResourceMapper, SysRoleResource> implements SysRoleResourceService {

}
