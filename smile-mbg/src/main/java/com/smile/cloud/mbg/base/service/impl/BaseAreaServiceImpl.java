package com.smile.cloud.mbg.base.service.impl;

import com.smile.cloud.mbg.base.model.BaseArea;
import com.smile.cloud.mbg.base.mapper.BaseAreaMapper;
import com.smile.cloud.mbg.base.service.BaseAreaService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 城市区域 服务实现类
 * </p>
 *
 * @author smile
 * @since 2020-12-31
 */
@Service
public class BaseAreaServiceImpl extends ServiceImpl<BaseAreaMapper, BaseArea> implements BaseAreaService {

}
