package com.smile.cloud.mbg.base.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author smile
 * @since 2020-12-31
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("sys_system")
@ApiModel(value="SysSystem对象", description="")
public class SysSystem implements Serializable {

    private static final long serialVersionUID = 8553239268806758736L;
    @TableId(value="saasCode",type = IdType.INPUT)
    @ApiModelProperty(value = "saas_code 系统唯一标识")
    private String saasCode;

    @ApiModelProperty(value = "saas系统名称")
    private String saasName;

    @ApiModelProperty(value = "区域id 关联base_area")
    private Integer areaId;

    @ApiModelProperty(value = "地址")
    private String address;

    @ApiModelProperty(value = "创建人")
    private Integer createBy;

    @ApiModelProperty(value = "创建时间")
    private Date createTime;

    @ApiModelProperty(value = "更新人")
    private Integer updateBy;

    @ApiModelProperty(value = "更新时间")
    private Date updateTime;


}
