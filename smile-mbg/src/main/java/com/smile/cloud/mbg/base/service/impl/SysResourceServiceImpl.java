package com.smile.cloud.mbg.base.service.impl;

import com.smile.cloud.mbg.base.model.SysResource;
import com.smile.cloud.mbg.base.mapper.SysResourceMapper;
import com.smile.cloud.mbg.base.service.SysResourceService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 资源表 服务实现类
 * </p>
 *
 * @author smile
 * @since 2020-12-31
 */
@Service
public class SysResourceServiceImpl extends ServiceImpl<SysResourceMapper, SysResource> implements SysResourceService {

}
