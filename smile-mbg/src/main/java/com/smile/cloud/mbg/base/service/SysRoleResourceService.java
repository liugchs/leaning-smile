package com.smile.cloud.mbg.base.service;

import com.smile.cloud.mbg.base.model.SysRoleResource;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 角色资源关系表 服务类
 * </p>
 *
 * @author smile
 * @since 2020-12-31
 */
public interface SysRoleResourceService extends IService<SysRoleResource> {

}
