package com.smile.cloud.mbg.base.mapper;

import com.smile.cloud.mbg.base.model.SysRoleResource;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 角色资源关系表 Mapper 接口
 * </p>
 *
 * @author smile
 * @since 2020-12-31
 */
public interface SysRoleResourceMapper extends BaseMapper<SysRoleResource> {

}
