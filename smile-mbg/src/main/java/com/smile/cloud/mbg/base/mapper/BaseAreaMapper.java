package com.smile.cloud.mbg.base.mapper;

import com.smile.cloud.mbg.base.model.BaseArea;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * <p>
 * 城市区域 Mapper 接口
 * </p>
 *
 * @author smile
 * @since 2020-12-31
 */
public interface BaseAreaMapper extends BaseMapper<BaseArea> {

}
