package com.smile.cloud.mbg.base.model;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 城市区域
 * </p>
 *
 * @author smile
 * @since 2020-12-31
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("base_area")
@ApiModel(value = "BaseArea对象", description = "城市区域")
public class BaseArea implements Serializable {

    private static final long serialVersionUID = 916239822025455585L;
    @ApiModelProperty(value = "主键")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "区域类型 例如：国、省、市、区（县）、镇")
    private String areaType;

    @ApiModelProperty(value = "区域名称 例如：中国、广东、珠海 、横琴镇")
    private String areaName;

    @ApiModelProperty(value = "上一级id")
    private Integer pid;


}
