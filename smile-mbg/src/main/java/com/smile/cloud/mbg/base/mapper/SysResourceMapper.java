package com.smile.cloud.mbg.base.mapper;

import com.smile.cloud.mbg.base.model.SysResource;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 资源表 Mapper 接口
 * </p>
 *
 * @author smile
 * @since 2020-12-31
 */
public interface SysResourceMapper extends BaseMapper<SysResource> {

}
