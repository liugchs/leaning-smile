package com.smile.cloud.mbg.base.service;

import com.smile.cloud.mbg.base.model.SysSystem;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author smile
 * @since 2020-12-31
 */
public interface SysSystemService extends IService<SysSystem> {

}
