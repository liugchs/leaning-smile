package com.smile.cloud.mbg.base.mapper;

import com.smile.cloud.mbg.base.model.SysUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 用户账号表 Mapper 接口
 * </p>
 *
 * @author smile
 * @since 2020-12-31
 */
public interface SysUserMapper extends BaseMapper<SysUser> {

}
