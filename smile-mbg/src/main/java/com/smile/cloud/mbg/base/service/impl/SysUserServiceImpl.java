package com.smile.cloud.mbg.base.service.impl;

import com.smile.cloud.mbg.base.model.SysUser;
import com.smile.cloud.mbg.base.mapper.SysUserMapper;
import com.smile.cloud.mbg.base.service.SysUserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户账号表 服务实现类
 * </p>
 *
 * @author smile
 * @since 2020-12-31
 */
@Service
public class SysUserServiceImpl extends ServiceImpl<SysUserMapper, SysUser> implements SysUserService {

}
