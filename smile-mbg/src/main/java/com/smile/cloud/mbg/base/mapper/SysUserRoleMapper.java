package com.smile.cloud.mbg.base.mapper;

import com.smile.cloud.mbg.base.model.SysUserRole;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 账号和角色关系表 Mapper 接口
 * </p>
 *
 * @author smile
 * @since 2020-12-31
 */
public interface SysUserRoleMapper extends BaseMapper<SysUserRole> {

}
