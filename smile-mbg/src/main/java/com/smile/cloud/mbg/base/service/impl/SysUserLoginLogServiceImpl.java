package com.smile.cloud.mbg.base.service.impl;

import com.smile.cloud.mbg.base.model.SysUserLoginLog;
import com.smile.cloud.mbg.base.mapper.SysUserLoginLogMapper;
import com.smile.cloud.mbg.base.service.SysUserLoginLogService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户登录日志表 服务实现类
 * </p>
 *
 * @author smile
 * @since 2020-12-31
 */
@Service
public class SysUserLoginLogServiceImpl extends ServiceImpl<SysUserLoginLogMapper, SysUserLoginLog> implements SysUserLoginLogService {

}
