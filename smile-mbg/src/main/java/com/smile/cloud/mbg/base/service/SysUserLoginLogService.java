package com.smile.cloud.mbg.base.service;

import com.smile.cloud.mbg.base.model.SysUserLoginLog;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 用户登录日志表 服务类
 * </p>
 *
 * @author smile
 * @since 2020-12-31
 */
public interface SysUserLoginLogService extends IService<SysUserLoginLog> {

}
