package com.smile.cloud.mbg.base.service.impl;

import com.smile.cloud.mbg.base.model.SysRole;
import com.smile.cloud.mbg.base.mapper.SysRoleMapper;
import com.smile.cloud.mbg.base.service.SysRoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户角色表 服务实现类
 * </p>
 *
 * @author smile
 * @since 2020-12-31
 */
@Service
public class SysRoleServiceImpl extends ServiceImpl<SysRoleMapper, SysRole> implements SysRoleService {

}
