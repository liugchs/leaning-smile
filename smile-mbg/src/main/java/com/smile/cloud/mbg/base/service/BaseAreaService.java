package com.smile.cloud.mbg.base.service;

import com.smile.cloud.mbg.base.model.BaseArea;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 城市区域 服务类
 * </p>
 *
 * @author smile
 * @since 2020-12-31
 */
public interface BaseAreaService extends IService<BaseArea> {

}
