package com.smile.cloud.points.service;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author LGC
 * @date 2021/8/12 17:32
 * @copyright 2021 mofang. All rights reserved
 */
@Data
public class OrderDTO implements Serializable {
    private static final long serialVersionUID = -4432388991480193975L;
    @ApiModelProperty(value = "订单ID")
    private String id;
    /**
     * 订单编号
     */
    @ApiModelProperty(value = "订单编号")
    private String orderNo;

    /**
     * 用户id
     */
    @ApiModelProperty(value = "用户id")
    private Integer userId;

    /**
     * 产品id
     */
    @ApiModelProperty(value = "产品id")
    private Integer productId;

    /**
     * 数量
     */
    @ApiModelProperty(value = "数量")
    private Integer count;

    /**
     * 金额
     */
    @ApiModelProperty(value = "金额")
    private BigDecimal money;
}
