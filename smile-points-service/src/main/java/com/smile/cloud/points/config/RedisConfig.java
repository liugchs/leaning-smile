package com.smile.cloud.points.config;

import com.smile.cloud.redisConfig.config.BaseRedisConfig;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Configuration;

/**
 * Redis相关配置
 *
 * @author smile
 */
@Configuration
@EnableCaching
public class RedisConfig extends BaseRedisConfig {

}
