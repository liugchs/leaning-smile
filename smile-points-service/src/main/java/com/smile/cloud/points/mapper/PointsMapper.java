package com.smile.cloud.points.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.smile.cloud.points.model.Points;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author LGC
 * @date 2021/8/12 18:46
 * @copyright 2021 mofang. All rights reserved
 */

public interface PointsMapper extends BaseMapper<Points> {
    int updateBatch(List<Points> list);

    int updateBatchSelective(List<Points> list);

    int batchInsert(@Param("list") List<Points> list);
}