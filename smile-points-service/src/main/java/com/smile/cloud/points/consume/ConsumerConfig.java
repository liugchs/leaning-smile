package com.smile.cloud.points.consume;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * @author LGC
 */
@Data
@Configuration
@ConfigurationProperties(prefix = "rocketmq.consumer")
public class ConsumerConfig {
    private String isOnOff;
    private String groupName;
    private String nameServerAddr;
    private String topics;
    private Integer consumeThreadMin;
    private Integer consumeThreadMax;
    private Integer consumeMessageBatchMaxSize;
}