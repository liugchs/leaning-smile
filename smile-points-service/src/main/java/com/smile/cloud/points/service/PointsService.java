package com.smile.cloud.points.service;

public interface PointsService {

    void increasePoints(OrderDTO order);
}
