package com.smile.cloud.points.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

import lombok.Data;

/**
 * @author LGC
 * @date 2021/8/13 10:40
 * @copyright 2021 mofang. All rights reserved
 */

@ApiModel(value = "com-smile-cloud-points-model-TRocketmqDedup")
@Data
@TableName(value = "t_rocketmq_dedup")
public class TRocketmqDedup implements Serializable {
    /**
     * 消费的应用名（可以用消费者组名称）
     */
    @TableField(value = "application_name")
    @ApiModelProperty(value = "消费的应用名（可以用消费者组名称）")
    private String applicationName;

    /**
     * 消息来源的topic（不同topic消息不会认为重复）
     */
    @TableField(value = "topic")
    @ApiModelProperty(value = "消息来源的topic（不同topic消息不会认为重复）")
    private String topic;

    /**
     * 消息的tag（同一个topic不同的tag，就算去重键一样也不会认为重复），没有tag则存""字符串
     */
    @TableField(value = "tag")
    @ApiModelProperty(value = "消息的tag（同一个topic不同的tag，就算去重键一样也不会认为重复），没有tag则存''字符串")
    private String tag;

    /**
     * 消息的唯一键（建议使用业务主键）
     */
    @TableField(value = "msg_uniq_key")
    @ApiModelProperty(value = "消息的唯一键（建议使用业务主键）")
    private String msgUniqKey;

    /**
     * 这条消息的消费状态
     */
    @TableField(value = "`status`")
    @ApiModelProperty(value = "这条消息的消费状态")
    private String status;

    /**
     * 这个去重记录的过期时间（时间戳）
     */
    @TableField(value = "expire_time")
    @ApiModelProperty(value = "这个去重记录的过期时间（时间戳）")
    private Long expireTime;

    private static final long serialVersionUID = 1L;
}