package com.smile.cloud.points.model;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @author LGC
 * @date 2021/8/12 18:46
 * @copyright 2021 mofang. All rights reserved
 */

/**
 * 积分
 */
@ApiModel(value = "com-smile-cloud-points-model-Points")
@Data
@TableName(value = "t_points")
public class Points implements Serializable {
    /**
     * 主键
     */
    @TableId(value = "id")
    @ApiModelProperty(value = "主键")
    private String id;

    /**
     * 用户id
     */
    @TableField(value = "user_id")
    @ApiModelProperty(value = "用户id")
    private Integer userId;

    /**
     * 订单编号
     */
    @TableField(value = "order_no")
    @ApiModelProperty(value = "订单编号")
    private String orderNo;

    /**
     * 积分
     */
    @TableField(value = "points")
    @ApiModelProperty(value = "积分")
    private Integer points;

    /**
     * 备注
     */
    @TableField(value = "remarks")
    @ApiModelProperty(value = "备注")
    private String remarks;

    private static final long serialVersionUID = 1L;
}