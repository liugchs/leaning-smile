package com.smile.cloud.points.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.smile.cloud.points.model.TRocketmqDedup;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 *
 * @author LGC
 * @date 2021/8/13 10:40
 * @copyright 2021 mofang. All rights reserved
 */

public interface TRocketmqDedupMapper extends BaseMapper<TRocketmqDedup> {
    int batchInsert(@Param("list") List<TRocketmqDedup> list);
}