package com.smile.cloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author LGC
 * @date 2021/8/12 17:56
 * @copyright 2021 mofang. All rights reserved
 */
@SpringBootApplication
public class PointsApplication {
    public static void main(String[] args) {
        SpringApplication.run(PointsApplication.class, args);
    }
}
