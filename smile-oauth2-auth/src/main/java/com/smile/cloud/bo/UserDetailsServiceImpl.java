package com.smile.cloud.bo;

import com.alibaba.nacos.common.utils.CollectionUtils;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.smile.cloud.mbg.base.model.SysRole;
import com.smile.cloud.mbg.base.model.SysUser;
import com.smile.cloud.mbg.base.model.SysUserRole;
import com.smile.cloud.mbg.base.service.SysRoleService;
import com.smile.cloud.mbg.base.service.SysUserRoleService;
import com.smile.cloud.mbg.base.service.SysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * 加载用户 信息 用户业务类
 * Created smile on  2020/6/19.
 */
@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private SysUserService sysUserService;
    @Autowired
    private SysUserRoleService sysUserRoleService;
    @Autowired
    private SysRoleService sysRoleService;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        SysUser sysUser = null;
        List<SysRole> roleList = new ArrayList<>();
        QueryWrapper<SysUser> query = new QueryWrapper<>();
        query.lambda()
                .eq(SysUser::getUuidAccount, username)
                .eq(SysUser::getEnableState, 1)
                .eq(SysUser::getIsDelete, 0);
        List<SysUser> sysUserList = sysUserService.list(query);
        if (CollectionUtils.isNotEmpty(sysUserList)) {
            sysUser = sysUserList.get(0);
        } else {
            throw new UsernameNotFoundException(String.format("用户'%s'不存在", username));
        }
        //从中间表查询用户角色
        QueryWrapper<SysUserRole> userRoleQueryWrapper = new QueryWrapper<>();
        userRoleQueryWrapper.lambda().eq(SysUserRole::getUserId, sysUser.getUserId());
        List<SysUserRole> userRoleList = sysUserRoleService.list(userRoleQueryWrapper);
        //从角色表获取角色
        if (CollectionUtils.isNotEmpty(userRoleList)) {
            Set<Integer> roleIdSet = userRoleList.stream().map(SysUserRole::getRoleId).collect(Collectors.toSet());
            List<SysRole> sysRoleList = sysRoleService.listByIds(roleIdSet);
            if (CollectionUtils.isNotEmpty(sysRoleList)) {
                for (SysRole xfpRole : sysRoleList) {
                    SysRole role = SysRole.builder()
                            .roleId(xfpRole.getRoleId())
                            .roleName(xfpRole.getRoleName())
                            .roleNo("role_" + xfpRole.getSaasCode() + "_" + xfpRole.getRoleNo())
                            .build();
                    roleList.add(role);
                }
            }
        }
        return new UserDetail(sysUser, roleList);
    }

}
