package com.smile.cloud.config;

import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Configuration;
import com.smile.cloud.redisConfig.config.BaseRedisConfig;


/**
 * Redis相关配置
 *
 * @author smile
 */
@Configuration
@EnableCaching
public class RedisConfig extends BaseRedisConfig {

}
