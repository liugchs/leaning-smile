package com.smile.cloud.gateway.filter;

import com.smile.cloud.gateway.constant.FilterOrderConstant;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.Ordered;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

public class TokenAuthenticationFilter implements GlobalFilter, Ordered {

    @Override
    public int getOrder() {
        // token 校验 优先级最高
        return FilterOrderConstant.MIN_LEVEL;
    }

    /**
     * 过滤器拦截
     */
    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
//        ServerHttpRequest request = exchange.getRequest();
//        URI uri = request.getURI();
//        if (StrUtil.contains(uri.getPath(), "login")) {
//            //登录页面
//            return chain.filter(exchange);
//        }
//        String token = request.getHeaders().getFirst(RequestConstan.REQ_TOkEN);
//        if (StrUtil.isNotBlank(token)) {
//            //校验身份通过,放行
//            RedisBaseUtil redisBaseUtil = SpringUtil.getBean(RedisBaseUtil.class);
//            if (redisBaseUtil.hHasKey(RedisLoginConstant.USER_LOGIN, token)) {
//                return chain.filter(exchange);
//            }
//        }
//        //校验身份不同步，返回登录过期，请重新登录
//        final ServerHttpResponse response = exchange.getResponse();
//        CommonResult<Object> fail = new CommonResult<>().fail(ResultEnum.RE_LONGIN);
//        byte[] bytes = JSONUtil.toJsonStr(fail).getBytes(StandardCharsets.UTF_8);
//        DataBuffer buffer = exchange.getResponse().bufferFactory().wrap(bytes);
//        response.getHeaders().setContentType(MediaType.APPLICATION_JSON);
//        return response.writeWith(Flux.just(buffer));
        return null;
    }

}