package com.smile.cloud.gateway.filter;


import com.smile.cloud.gateway.constant.FilterOrderConstant;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.gateway.filter.GatewayFilter;
import org.springframework.cloud.gateway.filter.factory.AbstractGatewayFilterFactory;
import org.springframework.core.Ordered;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class MenuAuthenticationFilter extends AbstractGatewayFilterFactory<MenuAuthenticationFilter.NameConfig> implements Ordered {

    @Override
    public int getOrder() {
        return FilterOrderConstant.TWENTY_LEVEL;
    }

    public MenuAuthenticationFilter() {
        super(NameConfig.class);
        log.info("Loaded GatewayFilterFactory [Authorize]");
    }

    @Override
    public GatewayFilter apply(NameConfig config) {
        return (exchange, chain) -> {
            System.out.println("=  =  =  =  =  = 执行了TEST=  =  =  =  =  =  =  =");
            return chain.filter(exchange);
        };
    }

    public static class NameConfig {

        private String name;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

    }
}