package com.smile.cloud.gateway.constant;


/**
 * @author LGC
 */
public class FilterOrderConstant {

    /**
     * 全局过滤器与局部过滤器级别是两个维度 统一 全局过滤器使用 0-19    局部 20以后
     */
    public static final int MIN_LEVEL = 0;

    public static final int ONE_LEVEL = 1;

    public static final int TWENTY_LEVEL = 20;

}
