package com.smile.cloud.admin.filter;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;

import java.io.Serializable;
import java.util.List;


/**
 * @author LGC
 */
@ApiModel(description = "JWT负载信息")
@Builder
@Data
public class PayloadDto implements Serializable {
    private static final long serialVersionUID = -8086201557426704875L;
    @ApiModelProperty("主题")
    private String sub;
    @ApiModelProperty("签发时间")
    private Long iat;
    @ApiModelProperty("过期时间")
    private Long exp;
    @ApiModelProperty("JWT的ID")
    private String jti;
    @ApiModelProperty("用户id")
    private Integer userId;
    @ApiModelProperty("用户电话")
    private String phone;
    @ApiModelProperty("用户拥有的权限")
    private List<String> authorities;
}