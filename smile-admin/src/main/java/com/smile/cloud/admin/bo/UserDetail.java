package com.smile.cloud.admin.bo;

import com.smile.cloud.mbg.base.model.SysRole;
import com.smile.cloud.mbg.base.model.SysUser;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * SpringSecurity需要的用户详情
 */
public class UserDetail implements UserDetails {
    private SysUser sysUser;
    private List<SysRole> roleList;

    public UserDetail(SysUser sysUser, List<SysRole> roleList) {
        this.sysUser = sysUser;
        this.roleList = roleList;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        //返回当前用户的角色
        List<GrantedAuthority> authorities = new ArrayList<>();
        for (SysRole role : this.roleList) {
            authorities.add(new SimpleGrantedAuthority(role.getRoleNo()));
        }
        return authorities;
    }

    @Override
    public String getPassword() {
        return sysUser.getPassword();
    }

    @Override
    public String getUsername() {
        return sysUser.getUuidAccount();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return sysUser.getEnableState().equals(1);
    }
}
