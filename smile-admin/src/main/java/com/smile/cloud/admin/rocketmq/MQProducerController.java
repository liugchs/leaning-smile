package com.smile.cloud.admin.rocketmq;

import com.smile.cloud.common.base.result.CommonResult;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.common.message.Message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * @author LGC
 */
@Slf4j
@RestController
@RequestMapping("/mqProducer")
public class MQProducerController {

    @Autowired
    private DefaultMQProducer defaultProducer;

    @GetMapping("/send")
    public CommonResult<SendResult> send(String msg) throws Exception {
        if (StringUtils.isEmpty(msg)) {
            return CommonResult.success();
        }
        log.info("发送MQ消息内容：" + msg);
        Message sendMsg = new Message("MyTopic", "MyTag", msg.getBytes());
        // 默认3秒超时
        SendResult sendResult = defaultProducer.send(sendMsg);
        return CommonResult.success(sendResult);
    }

}