package com.smile.cloud.admin.exception;

import com.smile.cloud.common.base.exception.BaseExceptionHandler;
import org.springframework.web.bind.annotation.ControllerAdvice;

/**
 * @author smile
 * @version V1.0
 * @className ExceptionHandler
 * @description TODO
 * @date 2021/6/29
 **/
@ControllerAdvice
public class ExceptionHandler extends BaseExceptionHandler {
}
