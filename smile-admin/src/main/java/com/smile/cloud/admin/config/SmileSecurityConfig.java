package com.smile.cloud.admin.config;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;

import com.smile.cloud.admin.service.AuthService;

import com.smile.cloud.mbg.base.model.SysResource;
import com.smile.cloud.mbg.base.model.SysRole;
import com.smile.cloud.mbg.base.model.SysRoleResource;
import com.smile.cloud.mbg.base.service.SysResourceService;
import com.smile.cloud.mbg.base.service.SysRoleResourceService;
import com.smile.cloud.mbg.base.service.SysRoleService;
import com.smile.cloud.security.component.DynamicSecurityService;
import com.smile.cloud.security.config.SecurityConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.access.ConfigAttribute;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.core.userdetails.UserDetailsService;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

/**
 * mall-security模块相关配置
 */
@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SmileSecurityConfig extends SecurityConfig {

    @Autowired
    private AuthService authService;
    @Autowired
    private SysRoleService sysRoleService;
    @Autowired
    private SysResourceService sysResourceService;
    @Autowired
    private SysRoleResourceService sysRoleResourceService;

    /**
     * 登录身份验证
     */
    @Override
    @Bean
    public UserDetailsService userDetailsService() {
        //获取登录用户信息
        return username -> authService.loadUserByUsername(username);
    }

    /**
     * 自定义实现资源角色加载
     *
     * @return
     */
    @Bean
    public DynamicSecurityService dynamicSecurityService() {
        return new DynamicSecurityService() {
            @Override
            public HashMap<String, Collection<ConfigAttribute>> loadDataSource() {
                HashMap<String, Collection<ConfigAttribute>> map = new HashMap<>(16);
                //获取所有资源与角色
                List<SysResource> resourcesList = sysResourceService.list();
                for (SysResource resource : resourcesList) {//遍历资源
                    QueryWrapper<SysRoleResource> query = new QueryWrapper<>();
                    query.lambda().eq(SysRoleResource::getResourceId, resource.getResourceId());
                    //查询资源对应的所有角色
                    List<SysRoleResource> roleResourcesList = sysRoleResourceService.list(query);
                    if (roleResourcesList != null && roleResourcesList.size() != 0) {
                        //获取对应的角色
                        List<Integer> roleIdList = roleResourcesList.stream().map(SysRoleResource::getRoleId).collect(Collectors.toList());
                        List<SysRole> xfpRoleList = sysRoleService.listByIds(roleIdList);

                        List<ConfigAttribute> roleList = new ArrayList<>();
                        for (SysRole sysRole : xfpRoleList) {
                            ConfigAttribute role = new org.springframework.security.access.SecurityConfig("role_" + sysRole.getSaasCode() + "_" + sysRole.getRoleNo());
                            roleList.add(role);
                        }
                        if (roleList.size() > 0) {
                            map.put(resource.getUrl(), roleList);
                        }
                    }
                }
                return map;
            }
        };
    }
}
