package com.smile.cloud.admin.controller.client;


import com.smile.cloud.rabbitConfig.service.OrderService;
import com.smile.cloud.rabbitConfig.util.RabbitTemplateUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 订单管理 RabbitMQ
 *
 * @author LGC
 */
@Controller
@Api(tags = "order")
@RequestMapping("/v1/api/client")
public class OrderController {
    @Autowired
    private OrderService orderService;
    @Autowired
    private RabbitTemplateUtil msgSenderUtil;

    @ApiOperation("/order/generateOrder")
    @PostMapping(value = "/order/generateOrder")
    @ResponseBody
    public Object generateOrder(@RequestBody Object orderParam) {
        orderService.generateOrder(orderParam);
        msgSenderUtil.sendMessage(orderParam);

        return 1;
    }
}
