package com.smile.cloud.admin.controller.client;

import com.smile.cloud.admin.scheduler.jobs.TaskJob;
import com.smile.cloud.admin.scheduler.utils.BaseQuartzUtil;
import com.smile.cloud.admin.vo.UserVo;
import com.smile.cloud.common.model.User;
import com.smile.cloud.common.base.result.CommonResult;
import com.smile.cloud.ossConfig.blob.BlobStorage;
import com.smile.cloud.ossConfig.blob.BlobUrlGenerator;
import com.smile.cloud.common.base.service.ConvertService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.BiConsumer;

/**
 * @author smile
 * @version V1.0
 * @className ClientController
 * @description TODO
 * @date 2021/1/8
 **/
@Api(tags = "第三方调用接口")
@RestController
@RequestMapping("/v1/api/client")
public class ClientController {


    @Autowired
    private ConvertService convertService;

    @Autowired
    private BaseQuartzUtil baseQuartzUtil;
    @Autowired
    private BlobStorage blobStorage;
    @Autowired
    private BlobUrlGenerator blobUrlGeneratorl;


    @PostMapping(value = "/blobStorage")
    @ApiOperation(value = "storage")
    private CommonResult<String> storage(MultipartFile file) throws Exception {
        blobStorage.create(file.getInputStream(), "demo/", file.getOriginalFilename());
        return CommonResult.success(blobUrlGeneratorl.url("demo/" + file.getOriginalFilename()));
    }


    @PostMapping(value = "/addWriteJob")
    @ApiOperation(value = "添加定时任务")
    private void addWriteJob(@Valid @RequestBody Object form) {
        Map<String, Object> extraParam = new HashMap<>();
        extraParam.put("task", "我是数据");
        extraParam.put("form", form);
        baseQuartzUtil.removeJob("com.smile.admin.scheduler.jobs.TaskJob");
        baseQuartzUtil.addCronJob("com.smile.admin.scheduler.jobs.TaskJob", "0/10 17 17 13 01 ? 2021", extraParam, TaskJob.class);
    }

    @GetMapping("/convert")
    @ApiOperation(value = "转换对象")
    public CommonResult<List<UserVo>> convert() throws Exception {

        List<User> users = new ArrayList<>();
        for (int i = 0; i < 9; i++) {
            users.add(User.builder().id(1).name(i + "").build());
        }
        List<UserVo> userVoList = convertService.convertList(users, UserVo.class, new BiConsumer<User, UserVo>() {
            @Override
            public void accept(User user, UserVo userVo) {
                userVo.setUserName("姓名：" + user.getName());
            }
        });
        return CommonResult.success(userVoList);
    }
}
