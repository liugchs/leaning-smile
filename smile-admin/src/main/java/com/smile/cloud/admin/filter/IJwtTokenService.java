package com.smile.cloud.admin.filter;



import com.nimbusds.jose.JOSEException;

import java.text.ParseException;

public interface IJwtTokenService {

    /**
     * 生成token
     *
     * @param payloadStr 负载json 字符串
     * @param secret     密钥
     * @return
     * @throws JOSEException
     */
    String generateTokenByHMAC(String payloadStr, String secret) throws JOSEException;

    /**
     * 验证token
     *
     * @param token
     * @param secret
     * @return
     * @throws ParseException
     * @throws JOSEException
     */
    PayloadDto verifyTokenByHMAC(String token, String secret) throws ParseException, JOSEException;

    /**
     * 默认负载信息
     *
     * @return
     */
    PayloadDto getDefaultPayloadDto();
}