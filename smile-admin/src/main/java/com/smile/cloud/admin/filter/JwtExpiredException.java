package com.smile.cloud.admin.filter;

/**
 * @author LGC
 * @date 2021/8/4 11:35
 * @copyright 2021 mofang. All rights reserved
 */
public class JwtExpiredException extends RuntimeException {

    private static final long serialVersionUID = -2061933185689540133L;

    public JwtExpiredException(String message) {
        super(message);
    }
    public JwtExpiredException() {
        super();
    }
}
