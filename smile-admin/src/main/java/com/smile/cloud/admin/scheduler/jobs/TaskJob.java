package com.smile.cloud.admin.scheduler.jobs;


import lombok.extern.slf4j.Slf4j;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.springframework.scheduling.quartz.QuartzJobBean;


@Slf4j
public class TaskJob extends QuartzJobBean {

    @Override
    protected void executeInternal(JobExecutionContext context) {
        JobDetail jobDetail = context.getJobDetail();
        /*获取Job的属性*/
        log.info("jobDetail's name : " + jobDetail.getKey().getName());
        log.info("jobDetail's group : " + jobDetail.getKey().getGroup());
        log.info("jobDetail's jobClassName : " + jobDetail.getJobClass().getName());
        log.info("jobDataMap's obj : " + jobDetail.getJobDataMap());
    }
}
