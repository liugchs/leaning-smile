package com.smile.cloud.admin.filter;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 不需要认证拦截
 *
 * @author LGC
 * @date 2021/8/4 10:34
 * @copyright 2021 mofang. All rights reserved
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface UnAuthInterceptor {
}
