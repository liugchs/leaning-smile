package com.smile.cloud.admin.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * @author LGC
 */
@Data
@ApiModel(value = "UserRegisterForm", description = "用户注册表单")
public class UserRegisterForm implements Serializable {

    private static final long serialVersionUID = -8626039619407102833L;
    @ApiModelProperty(value = "手机号", required = true)
    @NotBlank(message = "手机号不能为空")
    private String phone;

    @ApiModelProperty(value = "密码", required = true)
    @NotBlank(message = "密码不能为空")
    private String password;

    @ApiModelProperty(value = "saasCode", required = true)
    @NotBlank(message = "saasCode不能为空")
    private String saasCode;

}
