package com.smile.cloud.admin.vo;

import com.smile.cloud.mbg.base.model.SysResource;
import com.smile.cloud.mbg.base.model.SysRole;
import com.smile.cloud.mbg.base.model.SysUser;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author smile
 * @version V1.0
 * @className UserDetailVo
 * @description TODO
 * @date 2021/6/29
 **/
@Data
public class UserDetailVo implements Serializable {
    private static final long serialVersionUID = -6678228563000349547L;
    private SysUser user;
    private List<SysRole> roleList;
    private List<SysResource> resourceList;

}
