package com.smile.cloud.admin.scheduler.utils;

import cn.hutool.core.date.DateUtil;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Calendar;
import java.util.Date;

/**
 * @author smile
 * @version V1.0
 * @className CronUtil
 * @description cron工具
 * @date 2021/1/8
 **/
public class CronUtil {

    public static void main(String[] args) {
        String timeStr = "2021-01-13 16:12:00";
        Date date = DateUtil.parseDateTime(timeStr);
        System.out.println(getCron(date, 61));

        System.out.println(getCron(LocalDateTime.now()));
    }


    /**
     * 日期时间转化为cron表达式 ,可指定多少分钟再次执行（当天有效）
     *
     * @param localDateTime localDateTime
     * @param minutes       可指定多少分钟再次执行
     * @return
     */
    public static String getCron(LocalDateTime localDateTime, Integer minutes) {
        if (localDateTime == null) {
            return "";
        }
        ZoneId zoneId = ZoneId.systemDefault();
        ZonedDateTime zdt = localDateTime.atZone(zoneId);
        return getCron(Date.from(zdt.toInstant()), minutes);
    }


    /**
     * 日期时间转化为cron表达式 ,可指定多少分钟再次执行（当天有效）
     *
     * @param date    日期时间
     * @param minutes 可指定多少分钟再次执行
     * @return
     */
    public static String getCron(Date date, Integer minutes) {
        //从2020年3月28 15点20分开始 每五分钟执行一次
        //"0 20/5 15 28 3 ? 2020";
        //从2020年3月28 15点20分开始 每小时执行一次
        //"0 20 15/1 28 3 ? 2020";
        //从2020年3月28 15点20分开始 每天执行一次(网站解析失败)
        //"0 20 15 28/1 3 ? 2020";

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);

        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH) + 1;
        int day = calendar.get(Calendar.DATE);
        int house = calendar.get(Calendar.HOUR_OF_DAY);
        int minute = calendar.get(Calendar.MINUTE);

        Integer timingMinute = 0;
        Integer timingHour = 0;
        Integer timingDay = 0;
        Integer timingMonth = 0;

        //开始时间
        if (minutes / (60 * 24 * 31 * 12) >= 1) {
            //超过一年的 月份直接写到最大
            timingMinute = 12;
        } else if (0 < minutes / (60 * 24 * 31) && minutes / (60 * 24 * 31) < 12) {
            //月 （天的上限是31）
            timingMinute = minutes / (60 * 24 * 31);
        } else if (0 < minutes / ((60 * 24)) && minutes / ((60 * 24)) <= 31) {
            //天 (不支持24小时)
            timingDay = minutes / (60 * 24);
        } else if (0 < minutes / 60 && minutes / 60 <= 23) {
            //小时 （不支持60分 满60就记录一小时）
            timingHour = minutes / 60;
        } else {
            //分
            timingMinute = minutes;
        }


        String cron = String.format("0 %s/%s %s/%s %s/%s %s/%s ? %s", minute, timingMinute, house, timingHour, day, timingDay, month, timingMonth, year);
        cron = cron.replace("/0", "");
        return cron;
    }


    /**
     * 日期时间转化为cron表达式
     *
     * @param date
     * @return
     */
    public static String getCron(Date date) {
        String dateFormat = "ss mm HH dd MM ? yyyy";
        return DateUtil.format(date, dateFormat);
    }

    /**
     * 日期时间转化为cron表达式
     *
     * @param localDateTime
     * @return
     */
    public static String getCron(LocalDateTime localDateTime) {
        String dateFormat = "ss mm HH dd MM ? yyyy";
        return DateUtil.format(localDateTime, dateFormat);
    }


}
