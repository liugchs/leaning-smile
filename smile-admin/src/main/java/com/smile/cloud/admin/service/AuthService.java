package com.smile.cloud.admin.service;

import com.smile.cloud.admin.dto.UserRegisterForm;
import com.smile.cloud.admin.vo.UserDetailVo;
import com.smile.cloud.mbg.base.model.SysUser;
import org.springframework.security.core.userdetails.UserDetails;

/**
 * @ClassName UmsAdminService
 * @Description TODO
 * @author smile
 * @Date 2020/4/11
 * @Version V1.0
 **/
public interface AuthService {

    /**
     * 注册功能
     */
    SysUser register(UserRegisterForm userRegisterForm) throws Exception;

    /**
     * 登录功能
     *
     * @param username 用户名
     * @param password 密码
     * @return 生成的JWT的token
     */
    String login(String username, String password);


    UserDetailVo getUserDetailByToken(String token) throws Exception;

    /**
     * 刷新token的功能
     *
     * @param oldToken 旧的token
     */
    String refreshToken(String oldToken);

    /**
     * 获取用户信息
     */
    UserDetails loadUserByUsername(String username);

}
