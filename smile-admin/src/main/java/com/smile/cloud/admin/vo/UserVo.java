package com.smile.cloud.admin.vo;

import com.smile.cloud.mbg.base.model.SysRole;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.List;

/**
 * @author smile
 * @version V1.0
 * @className UserVo
 * @description TODO
 * @date 2021/6/2
 **/
@Data
public class UserVo implements Serializable {
    private static final long serialVersionUID = 4883251719647222473L;
    private Integer id;
    private String userName;
    private List<SysRole> roleList;
}
