package com.smile.cloud.admin.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;


@Data
@ApiModel(value = "UserLoginForm", description = "用户登录表单")
public class UserLoginForm implements Serializable {
    private static final long serialVersionUID = -6949511855406890878L;
    @ApiModelProperty(value = "saasCode", required = true)
    private String saasCode;

    @ApiModelProperty(value = "用户名", required = true)
    private String username;

    @ApiModelProperty(value = "密码", required = true)
    private String password;
}
