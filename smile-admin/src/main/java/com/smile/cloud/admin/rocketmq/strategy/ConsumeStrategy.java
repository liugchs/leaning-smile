package com.smile.cloud.admin.rocketmq.strategy;

import org.apache.rocketmq.common.message.MessageExt;

import java.util.function.Function;

/**
 */
public interface ConsumeStrategy {
     boolean invoke(Function<MessageExt, Boolean> consumeCallback, MessageExt messageExt);
}

