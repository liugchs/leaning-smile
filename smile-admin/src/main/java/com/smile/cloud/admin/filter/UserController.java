package com.smile.cloud.admin.filter;

import cn.hutool.crypto.SecureUtil;
import cn.hutool.json.JSONUtil;
import com.smile.cloud.common.base.result.CommonResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author LGC
 * @date 2021/8/2 14:20
 * @copyright 2021 mofang. All rights reserved
 */
@Slf4j
@Api(tags = "用户模块")
@RestController
@RequestMapping("/user")
public class UserController {

    @Resource
    private IJwtTokenService jwtTokenService;

    @UnAuthInterceptor
    @GetMapping("/getToken")
    @ApiOperation(value = "getToken")
    public CommonResult<String> getToken() {
        try {
            log.info("秘钥：{}", SecureUtil.md5("ssa-analyse"));
            CommonResult.success(jwtTokenService.generateTokenByHMAC(JSONUtil.toJsonStr(jwtTokenService.getDefaultPayloadDto()), SecureUtil.md5("ssa-analyse")));
        } catch (Exception e) {
            log.error("getToken--异常[{0}]", e);
        }
        return CommonResult.failed();
    }


}
