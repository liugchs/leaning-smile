package com.smile.cloud.api;

/**
 * 封装API的错误码
 * Created smile on  2019/4/19.
 */
public interface IErrorCode {
    long getCode();

    String getMessage();
}
