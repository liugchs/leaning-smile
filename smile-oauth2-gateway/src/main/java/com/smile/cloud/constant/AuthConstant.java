package com.smile.cloud.constant;

/**
 * Created smile on  2020/6/19.
 */
public class AuthConstant {

    public static final String AUTHORITY_PREFIX = "role_";

    public static final String AUTHORITY_CLAIM_NAME = "authorities";

}
