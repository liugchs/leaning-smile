package com.smile.cloud.authorization;

import org.springframework.security.access.ConfigAttribute;

import java.util.Collection;
import java.util.HashMap;

/**
 * 自定义实现资源角色加载
 */
public interface DynamicSecurityService {
    /**
     * 获取url对应的所用角色
     */
    HashMap<String, Collection<ConfigAttribute>> loadDataSource();
}
